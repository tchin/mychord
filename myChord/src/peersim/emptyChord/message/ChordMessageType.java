package peersim.emptyChord.message;

public class ChordMessageType {
	
	//join find successor message
	public static final int REQUESTJOIN = 0;
	public static final int REPLYJOIN = 1;
	public static final int FORWARDJOIN = 2;
	
	//fix finger find successor message
	//public static final int FIXFINGERSUCCESSORREQUEST = 3;
	//public static final int FIXFINGERSUCCESSORREPLY = 4;	
	//public static final int FIXFINGERSUCCESSORFORWARD = 5;	
	
	//notify message
	public static final int NOTIFY = 6;
	
	public static final int NOTIFYLEAVESUCCESSOR = 7;
	
	public static final int NOTIFYLEAVEPREDECESSOR  = 8;
	
	public static final int NOTIFYPREDECESSOR  = 9;
}
