package peersim.emptyChord.message;

import java.math.BigInteger;
import java.util.ArrayList;

import peersim.core.Node;

public class BasicForwardingMessage implements ChordMessage{
	protected Node sender;

	protected Node targetId;

	protected Node origin;

	protected int hopCounter = 0;

	protected int messageType;

	protected ArrayList<Node> forwardList;

	public BasicForwardingMessage(Node sender, Node targetId,
			 int messageType) {
		this.sender = sender;
		this.origin = sender;
		this.targetId = targetId;

		this.messageType = messageType;
		this.forwardList = new ArrayList();
		forwardList.add(sender);
	}

	public BasicForwardingMessage constructForword(Node targetId) {
		forwardList.add(targetId);
		this.sender = this.targetId;
		this.targetId = targetId;
		this.messageType = ChordMessageType.FORWARDJOIN;
		return this;
	}

	public BasicForwardingMessage constructReply() {
		int b = forwardList.size();
		this.sender = targetId;	
		this.targetId = forwardList.remove(forwardList.size() - 1);
	
		this.messageType = ChordMessageType.REPLYJOIN;
		return this;
	}

	public int getHopCounter() {
		return hopCounter;
	}

	public void increaseHopCounter() {
		this.hopCounter++;
	}


	public Node getSender() {
		return sender;
	}

	public int getMessageType() {
		return messageType;
	}

	public Node getOrigin() {
		return origin;
	}


	public Node getTargetId() {
		return targetId;
	}
}
