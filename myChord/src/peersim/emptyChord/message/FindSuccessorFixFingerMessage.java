package peersim.emptyChord.message;

import java.math.BigInteger;

import peersim.core.Node;

public class FindSuccessorFixFingerMessage extends FindSuccessorMessage {
	private int fingerPosition;
	public FindSuccessorFixFingerMessage(Node sender, Node targetId,
			BigInteger lookUpkey, int messageType, int fingerPosition) {
		super (sender, targetId, lookUpkey, messageType);
		this.fingerPosition = fingerPosition;
		
	}
	public int getFingerPosition() {
		return fingerPosition;
	}
	
}
