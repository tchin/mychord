package peersim.emptyChord.message;

import java.math.BigInteger;
import java.util.ArrayList;

import peersim.core.Node;
//message that is used to join a chord ring
//mesage type are forward join, reply join,
public class FindSuccessorMessage extends BasicForwardingMessage {


	private BigInteger lookUpKey;

	private Node successor;


	private Node predecessor;


	public FindSuccessorMessage(Node sender, Node targetId,
			BigInteger lookUpkey, int messageType) {
		super(sender, targetId,messageType);
		this.lookUpKey = lookUpkey;
	}


	public FindSuccessorMessage constructReply(Node successor) {
		int b = forwardList.size();
		this.sender = targetId;	
		this.targetId = forwardList.remove(forwardList.size() - 1);

		this.successor = successor;
		this.messageType = ChordMessageType.REPLYJOIN;
		return this;
	}
	public FindSuccessorMessage constructReply(Node successor, Node predecessor) {
		int b = forwardList.size();
		this.sender = targetId;	
		this.targetId = forwardList.remove(forwardList.size() - 1);

		this.successor = successor;
		this.predecessor = predecessor;
		this.messageType = ChordMessageType.REPLYJOIN;
		return this;
	}
	public BigInteger getLookUpKey() {
		return lookUpKey;
	}

	public Node getSuccessor() {
		return successor;
	}
	public Node getPredecessor(){
		return this.predecessor;
	}



}
