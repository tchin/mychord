package peersim.emptyChord.message;

import peersim.core.Node;

public class NotifySuccessor extends BasicForwardingMessage {
	Node successor;
	public NotifySuccessor(Node sender, Node targetId, Node successor, int messageType) {
		super(sender, targetId, messageType);
		// TODO Auto-generated constructor stub
		this.successor = successor;
	}
	public Node getSuccessor(){
		return successor;
	}

}
