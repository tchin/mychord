package peersim.emptyChord;

import java.math.BigInteger;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;

import peersim.config.Configuration;
import peersim.core.CommonState;
import peersim.core.Node;
import peersim.edsim.EDProtocol;
import peersim.emptyChord.message.BasicForwardingMessage;
import peersim.emptyChord.message.ChordMessageType;
import peersim.emptyChord.message.FindSuccessorFixFingerMessage;
import peersim.emptyChord.message.FindSuccessorMessage;
import peersim.emptyChord.message.NotifyLeave;
import peersim.emptyChord.message.NotifyMessage;
import peersim.emptyChord.observer.StabilizeObserver;
import peersim.transport.Transport;

public class EmptyChordProtocol implements EDProtocol {
	// Configuration parameter
	public String prefix;
	boolean flag = false;
	protected Parameters p;
	// Chord information
	protected BigInteger chordId;
	protected final int succLSize; // succlist size? not initialize how come
	protected final int idLength;
	public Node[] fingerTable;
	protected Node[] successorList;
	public Node predecessor;
	public boolean joinState;
	protected Node node;
	private final static BigInteger TWO = BigInteger.ONE.add(BigInteger.ONE);
	// information for finger table
	int currentF = 0;

	public void setNode(Node n) {
		this.node = n;
	}
	public int getIDLength(){
		return this.idLength;
	}
	// temperary variable to hold result from Event
	private Node closest;

	//information for statistical analysis
	
	public EmptyChordProtocol(String prefix) {
		super();
		// set prefix
		this.prefix = prefix;
		p = new Parameters();
		p.tid = Configuration
				.getPid(prefix + "." + PeersimConfig.PAR_TRANSPORT);
		idLength = Configuration.getInt(prefix + "."
				+ PeersimConfig.PAR_IDLENGTH);
		succLSize = Configuration.getInt(prefix + "."
				+ PeersimConfig.PAR_SUCCSIZE);
		this.chordId = ChordIdGenerator.generateID(idLength);
		fingerTable = new Node[idLength];
		successorList = new Node[succLSize];
		predecessor = null;
		joinState = false;
	}

	@Override
	public void processEvent(Node node, int pid, Object event) {
		// TODO Auto-generated method stub
		boolean test;
		if (this.node.isUp()) {
			if (successorList[0] ==null){
				System.out.println("**" + this.chordId.doubleValue());
				System.out.println("**" + this.joinState);
			}
			if (event.getClass() == NotifyLeave.class){
				if(joinState == false){
					return;
				}
				NotifyLeave message = (NotifyLeave) event;
				message.increaseHopCounter();
				switch (message.getMessageType()) {
				case ChordMessageType.NOTIFYLEAVESUCCESSOR:
					this.notifySuccessor(message.getOrigin());
					break;
				case ChordMessageType.NOTIFYLEAVEPREDECESSOR:
					this.notifyPredecessor(message.getOrigin());
					break;
				}
				
			} else if (event.getClass() == NotifyMessage.class){
				if(joinState == false){
					return;
				}
				NotifyMessage message = (NotifyMessage) event;
				message.increaseHopCounter();
				switch (message.getMessageType()) {
				case ChordMessageType.NOTIFY:
					this.notify(message.getOrigin());
					break;
				}
				
			}else if (event.getClass() == FindSuccessorFixFingerMessage.class) {
				if(joinState == false){
					return;
				}
				FindSuccessorFixFingerMessage message = (FindSuccessorFixFingerMessage) event;
				message.increaseHopCounter();
				switch (message.getMessageType()) {
				case ChordMessageType.REQUESTJOIN:
					if (idInab(message.getLookUpKey(), chordId,
							((EmptyChordProtocol) successorList[0]
									.getProtocol(pid)).getChordId(), idLength)) {
						// found successor
						Transport t = (Transport) this.node
								.getProtocol(getTranspotProtocol());
						FindSuccessorFixFingerMessage replyMessage = (FindSuccessorFixFingerMessage) message
								.constructReply(successorList[0]);
						t.send(this.node, message.getTargetId(), replyMessage,
								pid);
						// return successorList[0]
					} else {
						// send next message
						Transport t = (Transport) this.node
								.getProtocol(getTranspotProtocol());

						// getnext closest successor
						Node next = closestPrecedingNode(message.getLookUpKey());

						if (next != null) {
							FindSuccessorFixFingerMessage replyMessage = (FindSuccessorFixFingerMessage) message
									.constructForword(next);
							t.send(this.node, next, replyMessage, pid);
						}
					}
					break;
				case ChordMessageType.FORWARDJOIN:

					if (test = idInab(message.getLookUpKey(), chordId,
							((EmptyChordProtocol) successorList[0]
									.getProtocol(pid)).getChordId(), idLength)) {
						// found
						Transport t = (Transport) this.node
								.getProtocol(getTranspotProtocol());
						FindSuccessorFixFingerMessage replyMessage = (FindSuccessorFixFingerMessage) message
								.constructReply(successorList[0]);
						t.send(this.node, replyMessage.getTargetId(),
								replyMessage, pid);
						// return successorList[0]
					} else {
						// send next message

						Transport t = (Transport) this.node
								.getProtocol(getTranspotProtocol());

						// getnext closest successor
						Node next = closestPrecedingNode(message.getLookUpKey());
						
						if (next != null) {
							if (!next.equals(this.node)){
								FindSuccessorFixFingerMessage replyMessage = (FindSuccessorFixFingerMessage) message
										.constructForword(next);
								t.send(this.node, replyMessage.getTargetId(), replyMessage, pid);
							} else {
								if(message.getHopCounter() < 10){
									//try {
										//Thread.sleep(1000);
										FindSuccessorFixFingerMessage replyMessage = (FindSuccessorFixFingerMessage) message
												.constructForword(next);
										t.send(this.node, replyMessage.getTargetId(), replyMessage, pid);
									//} catch (InterruptedException e) {
										// TODO Auto-generated catch block
									//	e.printStackTrace();
									//}
								} else {
									System.out.println("=======================finger failure============================");
									System.out.println(message.getFingerPosition());
									System.out.println(((EmptyChordProtocol)(message.getOrigin().getProtocol(pid))).chordId.doubleValue());
									System.out.println(message.getLookUpKey().doubleValue());
								
								}
								
								
								System.out.println("=======================finger retry============================");
								
								
								
							}
							
						} else {
							System.out.println("=======================finger problem============================");
						}
					}
					break;		
				case ChordMessageType.REPLYJOIN:
					if (this.node.equals(message.getOrigin())) {
						// successfull reply of join message
						//KnockKnockClient.addNode(this.chordId.toString());
						//KnockKnockClient.connectNode(this.chordId.toString(),((EmptyChordProtocol) (message.getSuccessor()).getProtocol(pid)).chordId.toString());
						this.fingerTable[message.getFingerPosition()] = message.getSuccessor();


					} else {
						// keep forward back to original
						Transport t = (Transport) this.node
								.getProtocol(getTranspotProtocol());
						FindSuccessorFixFingerMessage replyMessage = (FindSuccessorFixFingerMessage) message
								.constructReply();
						t.send(this.node, replyMessage.getTargetId(),
								replyMessage, pid);
					}
					break;				
				}
			} else if (event.getClass() == FindSuccessorMessage.class) {
				FindSuccessorMessage message = (FindSuccessorMessage) event;
				message.increaseHopCounter();			
				switch (message.getMessageType()) {

				//starting to join netowrk message
				case ChordMessageType.REQUESTJOIN:
					if (idInab(message.getLookUpKey(), chordId,
							((EmptyChordProtocol) successorList[0]
									.getProtocol(pid)).getChordId(), idLength)) {
						// found successor
						Transport t = (Transport) this.node
								.getProtocol(getTranspotProtocol());
						FindSuccessorMessage replyMessage = message
								.constructReply(successorList[0]);
						t.send(this.node, message.getTargetId(), replyMessage,
								pid);
						// return successorList[0]
					} else {
						// send next message

						Transport t = (Transport) this.node
								.getProtocol(getTranspotProtocol());

						// getnext closest successor
						Node next = closestPrecedingNode(message.getLookUpKey());

						if (next != null) {
							FindSuccessorMessage replyMessage = (FindSuccessorMessage) message
									.constructForword(next);
							t.send(this.node, next, replyMessage, pid);
						}
					}
					break;
				case ChordMessageType.FORWARDJOIN:
					/*
					System.out.println("current message" + "FindSuccessorMessage");
					System.out.println("-" +message.getLookUpKey());
					System.out.println("+" +chordId.toString());
					System.out.println("#" +((EmptyChordProtocol) successorList[0]
							.getProtocol(pid)).getChordId());
					System.out.println("$" + ((EmptyChordProtocol) node
							.getProtocol(pid)).getChordId());
							*/
					if (test = idInab(message.getLookUpKey(), chordId,
							((EmptyChordProtocol) successorList[0]
									.getProtocol(pid)).getChordId(), idLength)) {
						// found
						Transport t = (Transport) this.node
								.getProtocol(getTranspotProtocol());
						FindSuccessorMessage replyMessage = message
								.constructReply(successorList[0]);
						t.send(this.node, replyMessage.getTargetId(),
								replyMessage, pid);
						// return successorList[0]
					} else {
						// send next message

						Transport t = (Transport) this.node
								.getProtocol(getTranspotProtocol());

						// getnext closest successor
						Node next = closestPrecedingNode(message.getLookUpKey());
						
						if (next != null) {
							if (!next.equals(this.node)){
								FindSuccessorMessage replyMessage = (FindSuccessorMessage) message
										.constructForword(next);
								t.send(this.node, replyMessage.getTargetId(), replyMessage, pid);
							} else {
								if(message.getHopCounter() < 10){
							
										FindSuccessorMessage replyMessage = (FindSuccessorMessage) message
												.constructForword(next);
										t.send(this.node, replyMessage.getTargetId(), replyMessage, pid);
									
								} else {
									System.out.println("=======================failure============================");
					
									System.out.println(((EmptyChordProtocol)(message.getOrigin().getProtocol(pid))).chordId.doubleValue());
									System.out.println(message.getLookUpKey().doubleValue());
								}
								
								
								System.out.println("=======================retry============================");
								
								
								
							}
							
						} else {
							System.out.println("=======================problem============================");
						}
					}
					break;
				case ChordMessageType.REPLYJOIN:
					String a = this.chordId.toString();
					String b = ((EmptyChordProtocol) message.getOrigin()
							.getProtocol(pid)).getChordId().toString();
					if (this.node.equals(message.getOrigin())) {
						if (message.getSuccessor().isUp()){
						// successfull reply of join message
						KnockKnockClient.addNode(this.chordId.toString());
						KnockKnockClient.connectNode(this.chordId.toString(),((EmptyChordProtocol) (message.getSuccessor()).getProtocol(pid)).chordId.toString());

						this.setSuccessor(message.getSuccessor());
						this.joinState = true;
						initFingerTable(message.getSuccessor());
						this.initializeSuccessList();
						// send next message
						if (!this.successorList[0].equals(this.node)){
						Transport t = (Transport) this.node
								.getProtocol(getTranspotProtocol());
						//NotifyMessage findMessage = new NotifyMessage(
						//		node, message.getSuccessor(), BigInteger.valueOf(0),
						//		ChordMessageType.NOTIFY);
						//t.send(this.node, message.getSuccessor(), findMessage, p.pid);
						}
						
						updateOthers();
						} else {
							System.out.println("join failure");
						}
					} else {
						// keep forward back to original
						Transport t = (Transport) this.node
								.getProtocol(getTranspotProtocol());
						FindSuccessorMessage replyMessage = (FindSuccessorMessage) message
								.constructReply();
						t.send(this.node, replyMessage.getTargetId(),
								replyMessage, pid);
					}
					break;

				}

			}
		}
	}

	// clone is call by the Dynamic Network to be added to network
	public Object clone() {
		EmptyChordProtocol cp = new EmptyChordProtocol(prefix);
		return cp;
	}

	public BigInteger getChordId() {
		return chordId;
	}

	public void cordIdReset() {
		this.chordId = new BigInteger(idLength, CommonState.r);
	}

	public int getTranspotProtocol() {
		return p.tid;
	}

	public void create(Node current) {
		this.predecessor = null;
		successorList[0] = current;
		for (int i = idLength; i > 0; i--) {

			fingerTable[i - 1] = current;

		}
		this.joinState = true;
		this.initializeSuccessList();
	}

	public void initFingerTable(Node succ) {
		this.fingerTable[0] = succ;
		for (int i = 1; i < fingerTable.length;i++)
			this.fingerTable[i] = ((EmptyChordProtocol)succ.getProtocol(p.pid)).fingerTable[i];
	}

	public void updateOthers() {

	}

	// exclude a include b
	public static boolean idInab(BigInteger id, BigInteger a, BigInteger b,
			int idLength) {
		//if (flag != true) {
		//	System.out.println("Comparing");
		//	flag = true;
		//}
	
		String x = id.toString();
		String y = a.toString();
		String z = b.toString();
		//a < b
		if (a.compareTo(b) < 0) {
			if (id.compareTo(b) <= 0 && id.compareTo(a) > 0) {
				return true;
			}
			// a > b
		} else if (a.compareTo(b) > 0) {
			// id > b
			if (id.compareTo(b) > 0) {
				// b + limit
				if (id.compareTo(b.add(TWO.pow(idLength))) < 0
						&& id.compareTo(a) > 0) {
					return true;
				}
				// id < b deleed && id.compareTo(a) > 0
			} else if (id.compareTo(b) < 0){
				if (id.add(TWO.pow(idLength)).compareTo(
						b.add(TWO.pow(idLength))) <= 0
						) {
					return true;
				}
			}
		} else {
			// since a is = b as a result is forever in the range
			return true;
		}
		if (b.compareTo(id) == 0) {
			return true;
		}
		return false;
	}

	// exclude a exclude b
	public static boolean idInab2(BigInteger id, BigInteger a, BigInteger b,
			int idLength) {
	while (id.compareTo(TWO.pow(idLength))>0){
			
			id = id.subtract(TWO.pow(idLength));
		
		}
		if (a.compareTo(b) < 0) {
			if (id.compareTo(b) < 0 && id.compareTo(a) > 0) {
				return true;
			}
			// b > a
		} else if (a.compareTo(b) > 0) {
			// id > b
			if (id.compareTo(b) > 0) {
				// b + limit
				if (id.compareTo(b.add(TWO.pow(idLength))) < 0
						&& id.compareTo(a) > 0) {
					return true;
				}
				// id < b
			} else if (id.compareTo(b) < 0){
				if (id.add(TWO.pow(idLength)).compareTo(
						b.add(TWO.pow(idLength))) < 0) {
					return true;
				}
			}
		} else {
			// a== b
			return true;
		}
		return false;
	}

	protected Node closestPrecedingNode(BigInteger id) {

		@SuppressWarnings("unused")
		Node current;
		for (int i = idLength; i > 0; i--) {

			if (fingerTable[i - 1] != null) {

				BigInteger fingerId = ((EmptyChordProtocol) (fingerTable[i - 1]
						.getProtocol(p.pid))).chordId;

				if (idInab2(fingerId, this.chordId, id, idLength)) {
					return fingerTable[i - 1];

				}
			}
		}

		return node;

	}

	public void fixFinger() {
		// Reset back next if more then the length of bit
		if (joinState == false){
			return ;
		}
		if (currentF >= idLength) {
			currentF = 0;
		}
		
		// if next finger is initialized skip fix and move next pointer to
		// next
		// finger entry
		//if (fingerTable[currentF] != null && fingerTable[currentF].isUp()) {
		//	currentF++;
		//	return;
		//}
		// varaible ussage
		BigInteger base;

		// calculate base according to next number
		if (currentF == 0)
			base = BigInteger.ONE;
		else {
			base = BigInteger.valueOf(2);
			for (int exp = 1; exp < currentF; exp++) {
				base = base.multiply(BigInteger.valueOf(2));
			}
		}

		// pot represent entry to be fix
		BigInteger pot = this.chordId.add(base);
		
		while (pot.compareTo(TWO.pow(idLength))>=0){
			pot = pot.subtract(TWO.pow(idLength));
			if (pot.compareTo(BigInteger.ZERO) == 0){
				break;
			}
		}
		Transport t = (Transport) this.node
				.getProtocol(getTranspotProtocol());
		FindSuccessorFixFingerMessage findMessage = new FindSuccessorFixFingerMessage(
				node, node, pot,
				ChordMessageType.REQUESTJOIN, currentF);

		t.send(this.node, this.node, findMessage, p.pid);
		
		
		//this.fingerTable[currentF] = findSuccessorFixFinger(pot, currentF);
		currentF++;
	}

	//find Successor for fix finger function
	public Node findSuccessorFixFinger(BigInteger key, int position) {
		
		boolean test;
		if (test = idInab(key, chordId,
				((EmptyChordProtocol) successorList[0].getProtocol(p.pid))
						.getChordId(), idLength)) {
			return successorList[0];
			// return successorList[0]
		} else {
			// send next message

			Transport t = (Transport) this.node
					.getProtocol(getTranspotProtocol());

			// getnext closest successor
			Node next = closestPrecedingNode(key);

			if (next != null) {
				FindSuccessorFixFingerMessage findMessage = new FindSuccessorFixFingerMessage(
						node, next, key,
						ChordMessageType.REQUESTJOIN, position);
				t.send(this.node, this.node, findMessage, p.pid);
			}
			return null;
		}
	}

	public Node[] getSuccessorList() {
		return successorList;
	}
	public void checkPredecessor(){
		if (joinState == false){
			return;
		}
		if (this.predecessor !=null){
			if (this.predecessor.isUp()){
				
			} else {
				this.predecessor = null;
			}
		}
	}
	public void notify(Node n){
		if (this.predecessor == null ){
			this.predecessor = n;
			return;
		} else if(idInab2(((EmptyChordProtocol)n.getProtocol(p.pid)).chordId,
				((EmptyChordProtocol)predecessor.getProtocol(p.pid)).chordId,this.chordId, this.idLength)){
			this.predecessor = n;
		} else {
			//System.out.println(((EmptyChordProtocol)n.getProtocol(p.pid)).chordId + "--" +
			//	((EmptyChordProtocol)predecessor.getProtocol(p.pid)).chordId+"%%" + this.chordId);
		}
	}
	
	public void notifySuccessor(Node n){
		this.predecessor = ((EmptyChordProtocol)n.getProtocol(p.pid)).predecessor;
		
	}
	public void notifyPredecessor(Node n){
		/*
		for (int i =0; i < successorList.length; i++){
			if (successorList[i].equals(n)){
				successorList[i] = ((EmptyChordProtocol)n.getProtocol(p.pid)).getSuccessorList()[i];
				if (i==0){
					KnockKnockClient.connectNode(this.chordId.toString(), ((EmptyChordProtocol)successorList[i].getProtocol(p.pid)).chordId.toString());
				}
				
			} else {

				System.out.println("TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT");
			}
			
			
			
		}
		*/
		
			if (successorList[0].equals(n)) {
					//this.successorList[0] = ;
					this.setSuccessor(((EmptyChordProtocol)n.getProtocol(p.pid)).getSuccessorList()[0]);
					KnockKnockClient.connectNode(this.chordId.toString(), ((EmptyChordProtocol)successorList[0].getProtocol(p.pid)).chordId.toString());
				} else {
			

		this.reconcilesSuccessList();
				}
	}

	public void stabilize(){
		if(!node.isUp()){
			return;
		}
		if (joinState == false){
			return;
		}
		if (this.successorList[0]!= null && successorList[0].isUp()){
			Node x = ((EmptyChordProtocol)this.successorList[0].getProtocol(p.pid)).predecessor;
			if(x != null){
				if (idInab(((EmptyChordProtocol)x.getProtocol(p.pid)).chordId,this.chordId,
						((EmptyChordProtocol)successorList[0].getProtocol(p.pid)).chordId, this.idLength)){
					KnockKnockClient.connectNode(this.chordId.toString(), ((EmptyChordProtocol)x.getProtocol(p.pid)).chordId.toString());
					//this.successorList[0] = x;
					setSuccessor(x);
					initFingerTable(x);
					
				}
				
			} 
			
			Transport t = (Transport) this.node
					.getProtocol(getTranspotProtocol());
			NotifyMessage findMessage = new NotifyMessage(
					node, successorList[0], ChordMessageType.NOTIFY);
			t.send(this.node, successorList[0], findMessage, p.pid);	
			
		} else if (!successorList[0].isUp()){
			for (int i = 1; i < successorList.length; i++){
				//this.successorList[0] = this.successorList[i];
				KnockKnockClient.connectNode(this.chordId.toString(),((EmptyChordProtocol) (this.successorList[i]).getProtocol(p.pid)).chordId.toString());
				setSuccessor(this.successorList[i]);

				if (successorList[0] != null  &&successorList[0].isUp()){
					reconcilesSuccessList();
					break;
				}
			}
		}
	}
	public void setSuccessor(Node n){
		if (((EmptyChordProtocol)n .getProtocol(p.pid)).joinState){
		this.successorList[0] = n;
		this.initializeSuccessList();
		initFingerTable(n);
		}

	}
	public void initializeSuccessList(){
		Node[]  successor;
		for (int i = 1; i < successorList.length; i++){
			successor = ((EmptyChordProtocol)successorList[i-1] .getProtocol(p.pid)).getSuccessorList();
			int count = 0;
			while (successor[0] == null){
				successor = ((EmptyChordProtocol)successorList[i-1] .getProtocol(p.pid)).getSuccessorList();
				System.out.println(count++);
				if (count >100){
					break;
				}
			}
			successorList[i] = successor[0];
		}		
	}
	public void reconcilesSuccessList(){
		if (this.successorList[0] != null){
			Node[]  successor = ((EmptyChordProtocol)successorList[0] .getProtocol(p.pid)).getSuccessorList();
			for (int i = 1; i < successorList.length; i++){
				this.successorList[i] = successor[i -1];
				//System.out.println("%" + ((EmptyChordProtocol)successorList[0] .getProtocol(p.pid)).chordId.doubleValue());
			}
		}
	}
}
