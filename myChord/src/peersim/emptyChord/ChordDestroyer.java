package peersim.emptyChord;

import peersim.config.Configuration;
import peersim.core.Node;
import peersim.dynamics.NodeInitializer;
import peersim.emptyChord.message.ChordMessageType;
import peersim.emptyChord.message.NotifyLeave;
import peersim.transport.Transport;

public class ChordDestroyer implements NodeInitializer {
	private int pid = 0;


	public ChordDestroyer(String prefix) {
		pid = Configuration.getPid(prefix + "." + PeersimConfig.PAR_PROT);

	}

	@Override
	public void initialize(Node n) {
		// TODO Auto-generated method stub
		Node successor = ((EmptyChordProtocol) n.getProtocol(pid))
				.getSuccessorList()[0];
		((EmptyChordProtocol) n.getProtocol(pid)).joinState = false;
		KnockKnockClient.deleteNode(((EmptyChordProtocol) n.getProtocol(pid))
				.getChordId().toString());
		KnockKnockClient.removeLine(((EmptyChordProtocol) n.getProtocol(pid))
				.getChordId().toString());
		Transport t = (Transport) n.getProtocol(((EmptyChordProtocol) n
				.getProtocol(pid)).getTranspotProtocol());
		if (successor != null) {

			NotifyLeave leaveMessage = new NotifyLeave(n, successor,
					ChordMessageType.NOTIFYLEAVESUCCESSOR);
			t.send(n, successor, leaveMessage, pid);
		} else {
			System.out.println("what is the damnnnnn node" + ((EmptyChordProtocol) n.getProtocol(pid)).getChordId().doubleValue() + 
					((EmptyChordProtocol) n.getProtocol(pid)).joinState);
		}
		Node predecessor = ((EmptyChordProtocol) n.getProtocol(pid)).predecessor;
		if (predecessor != null) {
			NotifyLeave leaveMessage2 = new NotifyLeave(n, predecessor,
					ChordMessageType.NOTIFYLEAVEPREDECESSOR);
			t.send(n, predecessor, leaveMessage2, pid);
		}
	}

}
