package peersim.emptyChord;

public class JoinProcess extends Thread {
    boolean pleaseWait = false;

    // This method is called when the thread runs
    public void run() {
        while (true) {
            // Do work
        	System.out.println(" i love you");
            // Check if should wait
            synchronized (this) {
                while (pleaseWait) {
                    try {
                        wait();
                    } catch (Exception e) {
                    }
                }
            }

            // Do work
        }
    }
}
