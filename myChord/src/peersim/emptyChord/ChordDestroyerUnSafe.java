package peersim.emptyChord;

import peersim.config.Configuration;
import peersim.core.Node;
import peersim.dynamics.NodeInitializer;

public class ChordDestroyerUnSafe  implements NodeInitializer {
	private int pid = 0;
	public ChordDestroyerUnSafe(String prefix) {
		pid = Configuration.getPid(prefix + "." + PeersimConfig.PAR_PROT);

	}
	@Override
	public void initialize(Node n) {
		// TODO Auto-generated method stub
		((EmptyChordProtocol)n.getProtocol(pid)).joinState = false;
		KnockKnockClient.deleteNode(((EmptyChordProtocol)n.getProtocol(pid)).getChordId().toString());
		KnockKnockClient.removeLine(((EmptyChordProtocol)n.getProtocol(pid)).getChordId().toString());
		
		
	}

}
