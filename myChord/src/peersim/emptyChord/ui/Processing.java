package peersim.emptyChord.ui;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;


public class Processing extends Thread{
	Socket clientSocket;
	MainUIController mainUIController;
	public Processing(Socket clientSocket , MainUIController mainUIController) {
		super();
		this.clientSocket = clientSocket;
		this.mainUIController = mainUIController;
	}
	public void run(){
		PrintWriter out;
		try {
			out = new PrintWriter(clientSocket.getOutputStream(), true);
			BufferedReader in = new BufferedReader(new InputStreamReader(
					clientSocket.getInputStream()));
			
			String inputLine, outputLine;

			// initiate conversation with client
			KnockKnockProtocol kkp = new KnockKnockProtocol(mainUIController);
			outputLine = kkp.processInput(null);
			out.println(outputLine);

			while ((inputLine = in.readLine()) != null) {
				outputLine = kkp.processInput(inputLine);
				out.println(outputLine);
				if (outputLine.equals("Bye."))
					break;
			}		
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
