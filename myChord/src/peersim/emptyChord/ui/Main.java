package peersim.emptyChord.ui;


import java.awt.Toolkit;
import java.io.IOException;
import java.math.BigInteger;
import java.net.ServerSocket;
import java.net.URL;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.effect.BlendMode;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.CircleBuilder;
import javafx.scene.shape.Ellipse;
import javafx.scene.shape.EllipseBuilder;
import javafx.scene.text.Text;
import javafx.scene.text.TextBuilder;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

public class Main extends Application {
	private final static BigInteger TWO = BigInteger.ONE.add(BigInteger.ONE);
	public static double screenWidth = Toolkit.getDefaultToolkit().getScreenSize().getWidth();

	public static double screenHeight = Toolkit.getDefaultToolkit().getScreenSize()
			.getHeight();
	
	public static int idLength;
	
	public static double radius ;
	@Override
	public void start(final Stage primaryStage) {

		URL locationPlayer = getClass().getResource("MainUI.fxml");
		FXMLLoader fxmlLoaderPlayer = new FXMLLoader(locationPlayer, null);
		MainUIController mainControl;
		AnchorPane mainPanel = null;
		try {
			mainPanel = (AnchorPane) fxmlLoaderPlayer.load();
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		mainControl = (MainUIController) fxmlLoaderPlayer.getController();
		primaryStage.setTitle("chord visualizaion");

		double x =  (screenWidth /2);
		double y =  (screenHeight /2);
		Parameters param = getParameters();
		idLength = Integer.parseInt(param.getRaw().get(0));
		
		//fix radius
		radius =TWO.pow(6).doubleValue()* 2 /2 * Math.PI ;
		mainControl.setRadius(radius);
		mainControl.startServer();
		Scene scene = new Scene(mainPanel, 300, 200, Color.WHITE);
		Ellipse ellipse = EllipseBuilder.create().centerX(x).centerY(y)
				.radiusX(radius).radiusY(radius).strokeWidth(1).stroke(Color.BLACK)
				.fill(Color.WHITE).build();
		ellipse.setBlendMode(BlendMode.MULTIPLY);
		mainPanel.getChildren().add(ellipse);
		primaryStage.setScene(scene);
		primaryStage.setOnHiding(new EventHandler<WindowEvent>() {
			@Override
			public void handle(WindowEvent event) {
				// TODO Auto-generated method stub
				primaryStage.setOnHiding(null);
                primaryStage.hide();
				Platform.exit();
				System.exit(0);
			}
		});
		primaryStage.show();
	}

	public static void main(String[] args) {
		launch(args);
	}
}
