package peersim.emptyChord.ui.message;

public class ConnectNodeMessage  extends ObjectUIMessage{
	public String from;
	public String to;
	public ConnectNodeMessage(String from, String to) {
		super();
		this.from = from;
		this.to = to;
	}
	
	
}
