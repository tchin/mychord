package peersim.emptyChord.ui.message;

public class RemoveLineMessage extends ObjectUIMessage{
	public String from;

	public RemoveLineMessage(String from) {
		super();
		this.from = from;
	}
	
}
