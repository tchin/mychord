package peersim.emptyChord.ui.message;

public class AddNodeMessage extends ObjectUIMessage{
	public String Server;

	public AddNodeMessage(String server) {
		super();
		Server = server;
	}
	
}
