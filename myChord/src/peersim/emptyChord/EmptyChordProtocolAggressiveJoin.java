package peersim.emptyChord;

import java.math.BigInteger;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;

import peersim.config.Configuration;
import peersim.core.CommonState;
import peersim.core.Node;
import peersim.edsim.EDProtocol;
import peersim.emptyChord.message.BasicForwardingMessage;
import peersim.emptyChord.message.ChordMessageType;
import peersim.emptyChord.message.FindSuccessorFixFingerMessage;
import peersim.emptyChord.message.FindSuccessorMessage;
import peersim.emptyChord.message.NotifyLeave;
import peersim.emptyChord.message.NotifyMessage;
import peersim.emptyChord.message.NotifySuccessor;
import peersim.emptyChord.observer.StabilizeObserver;
import peersim.transport.Transport;

public class EmptyChordProtocolAggressiveJoin extends EmptyChordProtocol implements EDProtocol {


	public EmptyChordProtocolAggressiveJoin(String prefix) {
		super(prefix);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void processEvent(Node node, int pid, Object event) {
		// TODO Auto-generated method stub
		boolean test;
		if (this.node.isUp()) {
			if (successorList[0] ==null){
				System.out.println("**" + this.chordId.doubleValue());
				System.out.println("**" + this.joinState);
			}
			if (event.getClass() == NotifySuccessor.class){
				if(joinState == false){
					return;
				}
				NotifySuccessor message = (NotifySuccessor) event;
				message.increaseHopCounter();
				switch (message.getMessageType()) {

				case ChordMessageType.NOTIFYPREDECESSOR:
					this.notifyPredecessor2(message.getSuccessor());
					break;
				}
				
			} else if (event.getClass() == NotifyLeave.class){
				if(joinState == false){
					return;
				}
				NotifyLeave message = (NotifyLeave) event;
				message.increaseHopCounter();
				switch (message.getMessageType()) {
				case ChordMessageType.NOTIFYLEAVESUCCESSOR:
					this.notifySuccessor(message.getOrigin());
					break;
				case ChordMessageType.NOTIFYLEAVEPREDECESSOR:
					this.notifyPredecessor(message.getOrigin());
					break;
				}
				
			} else if (event.getClass() == NotifyMessage.class){
				if(joinState == false){
					return;
				}
				NotifyMessage message = (NotifyMessage) event;
				message.increaseHopCounter();
				switch (message.getMessageType()) {
				case ChordMessageType.NOTIFY:
					this.notify(message.getOrigin());
					break;
				}
				
			}else if (event.getClass() == FindSuccessorFixFingerMessage.class) {
				if(joinState == false){
					return;
				}
				FindSuccessorFixFingerMessage message = (FindSuccessorFixFingerMessage) event;
				message.increaseHopCounter();
				switch (message.getMessageType()) {
				case ChordMessageType.REQUESTJOIN:
					if (idInab(message.getLookUpKey(), chordId,
							((EmptyChordProtocolAggressiveJoin) successorList[0]
									.getProtocol(pid)).getChordId(), idLength)) {
						// found successor
						Transport t = (Transport) this.node
								.getProtocol(getTranspotProtocol());
						FindSuccessorFixFingerMessage replyMessage = (FindSuccessorFixFingerMessage) message
								.constructReply(successorList[0]);
						t.send(this.node, message.getTargetId(), replyMessage,
								pid);
						// return successorList[0]
					} else {
						// send next message
						Transport t = (Transport) this.node
								.getProtocol(getTranspotProtocol());

						// getnext closest successor
						Node next = closestPrecedingNode(message.getLookUpKey());

						if (next != null) {
							FindSuccessorFixFingerMessage replyMessage = (FindSuccessorFixFingerMessage) message
									.constructForword(next);
							t.send(this.node, next, replyMessage, pid);
						}
					}
					break;
				case ChordMessageType.FORWARDJOIN:

					if (test = idInab(message.getLookUpKey(), chordId,
							((EmptyChordProtocolAggressiveJoin) successorList[0]
									.getProtocol(pid)).getChordId(), idLength)) {
						// found
						Transport t = (Transport) this.node
								.getProtocol(getTranspotProtocol());
						FindSuccessorFixFingerMessage replyMessage = (FindSuccessorFixFingerMessage) message
								.constructReply(successorList[0]);
						t.send(this.node, replyMessage.getTargetId(),
								replyMessage, pid);
						// return successorList[0]
					} else {
						// send next message

						Transport t = (Transport) this.node
								.getProtocol(getTranspotProtocol());

						// getnext closest successor
						Node next = closestPrecedingNode(message.getLookUpKey());
						
						if (next != null) {
							if (!next.equals(this.node)){
								FindSuccessorFixFingerMessage replyMessage = (FindSuccessorFixFingerMessage) message
										.constructForword(next);
								t.send(this.node, replyMessage.getTargetId(), replyMessage, pid);
							} else {
								if(message.getHopCounter() < 10){
									//try {
										//Thread.sleep(1000);
										FindSuccessorFixFingerMessage replyMessage = (FindSuccessorFixFingerMessage) message
												.constructForword(next);
										t.send(this.node, replyMessage.getTargetId(), replyMessage, pid);
									//} catch (InterruptedException e) {
										// TODO Auto-generated catch block
									//	e.printStackTrace();
									//}
								} else {
									System.out.println("=======================finger failure============================");
									System.out.println(message.getFingerPosition());
									System.out.println(((EmptyChordProtocolAggressiveJoin)(message.getOrigin().getProtocol(pid))).chordId.doubleValue());
									System.out.println(message.getLookUpKey().doubleValue());
								
								}
								
								
								System.out.println("=======================finger retry============================");
								
								
								
							}
							
						} else {
							System.out.println("=======================finger problem============================");
						}
					}
					break;		
				case ChordMessageType.REPLYJOIN:
					if (this.node.equals(message.getOrigin())) {
						// successfull reply of join message
						//KnockKnockClient.addNode(this.chordId.toString());
						//KnockKnockClient.connectNode(this.chordId.toString(),((EmptyChordProtocol) (message.getSuccessor()).getProtocol(pid)).chordId.toString());
						this.fingerTable[message.getFingerPosition()] = message.getSuccessor();


					} else {
						// keep forward back to original
						Transport t = (Transport) this.node
								.getProtocol(getTranspotProtocol());
						FindSuccessorFixFingerMessage replyMessage = (FindSuccessorFixFingerMessage) message
								.constructReply();
						t.send(this.node, replyMessage.getTargetId(),
								replyMessage, pid);
					}
					break;				
				}
			} else if (event.getClass() == FindSuccessorMessage.class) {
				FindSuccessorMessage message = (FindSuccessorMessage) event;
				message.increaseHopCounter();			
				switch (message.getMessageType()) {

				//starting to join netowrk message
				case ChordMessageType.REQUESTJOIN:
					if (idInab(message.getLookUpKey(), chordId,
							((EmptyChordProtocolAggressiveJoin) successorList[0]
									.getProtocol(pid)).getChordId(), idLength)) {
						// found successor
						Node old = ((EmptyChordProtocolAggressiveJoin) successorList[0]
								.getProtocol(pid)).predecessor;
						Transport t = (Transport) this.node
								.getProtocol(getTranspotProtocol());
						
						 ((EmptyChordProtocolAggressiveJoin) successorList[0]
									.getProtocol(pid)).predecessor = message.getOrigin();
						 
						FindSuccessorMessage replyMessage = message
								.constructReply(successorList[0], old);
						t.send(this.node, message.getTargetId(), replyMessage,
								pid);

						if (old != null) {
							NotifySuccessor leaveMessage2 = new NotifySuccessor(this.node, old,
									message.getOrigin(),
									ChordMessageType.NOTIFYPREDECESSOR);
							t.send(this.node, old, leaveMessage2, pid);
							
						}
						// return successorList[0]
					} else {
						// send next message

						Transport t = (Transport) this.node
								.getProtocol(getTranspotProtocol());

						// getnext closest successor
						Node next = closestPrecedingNode(message.getLookUpKey());

						if (next != null) {
							FindSuccessorMessage replyMessage = (FindSuccessorMessage) message
									.constructForword(next);
							t.send(this.node, next, replyMessage, pid);
							
						}
					}
					break;
				case ChordMessageType.FORWARDJOIN:
					/*
					System.out.println("current message" + "FindSuccessorMessage");
					System.out.println("-" +message.getLookUpKey());
					System.out.println("+" +chordId.toString());
					System.out.println("#" +((EmptyChordProtocol) successorList[0]
							.getProtocol(pid)).getChordId());
					System.out.println("$" + ((EmptyChordProtocol) node
							.getProtocol(pid)).getChordId());
							*/
					if (this.joinState == false){
						return;
					}
					if (test = idInab(message.getLookUpKey(), chordId,
							((EmptyChordProtocolAggressiveJoin) successorList[0]
									.getProtocol(pid)).getChordId(), idLength)) {
						// found
						Transport t = (Transport) this.node
								.getProtocol(getTranspotProtocol());
						Node old = ((EmptyChordProtocolAggressiveJoin) successorList[0]
								.getProtocol(pid)).predecessor;
						
						((EmptyChordProtocolAggressiveJoin) successorList[0]
								.getProtocol(pid)).predecessor = message.getOrigin();
						
						FindSuccessorMessage replyMessage = message
								.constructReply(successorList[0], old);
						t.send(this.node, replyMessage.getTargetId(),
								replyMessage, pid);
						if (old != null) {
							NotifySuccessor leaveMessage2 = new NotifySuccessor(this.node, old,
									message.getOrigin(),
									ChordMessageType.NOTIFYPREDECESSOR);
							t.send(this.node, old, leaveMessage2, pid);
						}
						
						// return successorList[0]
					} else {
						// send next message

						Transport t = (Transport) this.node
								.getProtocol(getTranspotProtocol());

						// getnext closest successor
						Node next = closestPrecedingNode(message.getLookUpKey());
						
						if (next != null) {
							if (!next.equals(this.node)){
								FindSuccessorMessage replyMessage = (FindSuccessorMessage) message
										.constructForword(next);
								t.send(this.node, replyMessage.getTargetId(), replyMessage, pid);
							} else {
								if(message.getHopCounter() < 10){
							
										FindSuccessorMessage replyMessage = (FindSuccessorMessage) message
												.constructForword(next);
										t.send(this.node, replyMessage.getTargetId(), replyMessage, pid);
									
								} else {
									System.out.println("=======================failure============================");
					
									System.out.println(((EmptyChordProtocolAggressiveJoin)(message.getOrigin().getProtocol(pid))).chordId.doubleValue());
									System.out.println(message.getLookUpKey().doubleValue());
								}
								
								
								System.out.println("=======================retry============================");
								
								
								
							}
							
						} else {
							System.out.println("=======================problem============================");
						}
					}
					break;
				case ChordMessageType.REPLYJOIN:
					String a = this.chordId.toString();
					String b = ((EmptyChordProtocolAggressiveJoin) message.getOrigin()
							.getProtocol(pid)).getChordId().toString();
					if (this.node.equals(message.getOrigin())) {
						if (message.getSuccessor().isUp()){
						// successfull reply of join message
						KnockKnockClient.addNode(this.chordId.toString());
						KnockKnockClient.connectNode(this.chordId.toString(),((EmptyChordProtocolAggressiveJoin) (message.getSuccessor()).getProtocol(pid)).chordId.toString());
						//this.successorList[0] = message.getSuccessor();
						setSuccessor(message.getSuccessor());
						this.predecessor = message.getPredecessor();

						this.joinState = true;
						initFingerTable(message.getSuccessor());
						this.initializeSuccessList();
						// send next message
						if (!this.successorList[0].equals(this.node)){
						Transport t = (Transport) this.node
								.getProtocol(getTranspotProtocol());
						//NotifyMessage findMessage = new NotifyMessage(
						//		node, message.getSuccessor(), BigInteger.valueOf(0),
						//		ChordMessageType.NOTIFY);
						//t.send(this.node, message.getSuccessor(), findMessage, p.pid);
						}
						
						updateOthers();
						} else {
							System.out.println("join failure");
						}
					} else {
						// keep forward back to original
						Transport t = (Transport) this.node
								.getProtocol(getTranspotProtocol());
						FindSuccessorMessage replyMessage = (FindSuccessorMessage) message
								.constructReply();
						t.send(this.node, replyMessage.getTargetId(),
								replyMessage, pid);
					}
					break;

				}

			}
		}
	}

	// clone is call by the Dynamic Network to be added to network
	public Object clone() {
		EmptyChordProtocolAggressiveJoin cp = new EmptyChordProtocolAggressiveJoin(prefix);
		return cp;
	}
	public void notifyPredecessor2(Node n){
		//System.out.println(((EmptyChordProtocol)n.getProtocol(p.pid)).chordId+"%"+chordId+"+"+((EmptyChordProtocol) successorList[0]
		//		.getProtocol(p.pid)).getChordId());
		//KnockKnockClient.connectNode(this.chordId.toString(),((EmptyChordProtocol)n.getProtocol(p.pid)).chordId.toString());
	
		if (idInab(((EmptyChordProtocol)n.getProtocol(p.pid)).chordId,chordId,((EmptyChordProtocol) successorList[0]
				.getProtocol(p.pid)).getChordId()
					, idLength)) {
				//this.successorList[0] = n;
				setSuccessor(n);
				KnockKnockClient.connectNode(this.chordId.toString(),((EmptyChordProtocol)n.getProtocol(p.pid)).chordId.toString());
			}
		

		this.reconcilesSuccessList();
	}
}
