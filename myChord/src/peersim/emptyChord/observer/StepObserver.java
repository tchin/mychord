package peersim.emptyChord.observer;

import peersim.config.Configuration;
import peersim.core.Control;
import peersim.core.Network;
import peersim.core.Node;
import peersim.emptyChord.EmptyChordProtocol;

public class StepObserver implements Control {
	private final String prefix;
	private static final String PAR_PROT = "protocol";
	private final int pid;
	static MyWriter w;

	public StepObserver(String prefix) {
		this.prefix = prefix;
		this.pid = Configuration.getPid(prefix + "." + PAR_PROT);
		w = new MyWriter("out3.txt");
	}

	@Override
	public boolean execute() {
		// TODO Auto-generated method stub
		// System.out.println(Network.size());
		printAllNode();
		return false;
	}

	private void printAllNode() {
		w.write("\nBegin\n");
		for (int i = 0; i < 1000; i++) {

			try {
				Node node = (Node) Network.get(i);
				if (node != null) {
					EmptyChordProtocol cp = ((EmptyChordProtocol) node
							.getProtocol(pid));
					w.write(node + "chorid id=" + cp.getChordId() + " stablilize="
							+ cp.joinState + "\n");
					if (cp.predecessor != null) {
						w.write("\n "
								+ cp.getChordId()
								+ "link to successor "
								+ ((EmptyChordProtocol) cp.getSuccessorList()[0]
										.getProtocol(pid)).getChordId()
								+ "and predessor are"
								+ (((EmptyChordProtocol) cp.predecessor
										.getProtocol(pid)).getChordId()
										+ "and is up =" + cp.predecessor.isUp() + "\n"));
					} else {
						w.write("\n "
								+ cp.getChordId()
								+ "link to successor "
								+ ((EmptyChordProtocol) cp.getSuccessorList()[0]
										.getProtocol(pid)).getChordId()
								+ "and predessor are null" + "\n");
						
					}
					
					w.write("\nfiger table for Node " + i +"\n");
					for (int j = cp.fingerTable.length - 1; j > 0; j--) {
						if (cp.fingerTable[j] == null) {
							w.write("Finger " + j + " is null\n");
							continue;
						}
						w.write("Finger["
										+ j
										+ "] = "
										+ cp.fingerTable[j].getIndex()
										+ " chordId "
										+ ((EmptyChordProtocol) cp.fingerTable[j]
												.getProtocol(pid)).getChordId() +"\n");
					}	
								
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block

			}
		}
		w.write("\nEnd\n");
	}

}
