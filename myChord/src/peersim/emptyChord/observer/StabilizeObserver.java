package peersim.emptyChord.observer;

import java.util.Date;

import peersim.config.Configuration;
import peersim.core.Control;
import peersim.emptyChord.message.NotifyMessage;

public class StabilizeObserver implements Control{
	static MyWriter  w = new MyWriter("stabilizeResult.txt");
	private final String prefix;
	private static final String PAR_PROT = "protocol";
	private final int pid;
	public  static long startTime = 0;
	public  static long endTime = 0;
	public static long now = 0;
	public static long pre = 0;
	public StabilizeObserver(String prefix) {
		this.prefix = prefix;
		this.pid = Configuration.getPid(prefix + "." + PAR_PROT);

	}
	
	public static void writeResult(){
		w.write((endTime - startTime) / 1000.0 +"\n");
	}
	@Override
	public boolean execute() {
		// TODO Auto-generated method stub

		return false;
	}
	public static void stabilize(){
		if (startTime == 0){
			startTime = System.currentTimeMillis();
		} else {
			endTime = System.currentTimeMillis();
		}
		now = (long) ((endTime - startTime) / 1000.0);
		if (now - pre >= 1){
		System.out.println((endTime - startTime) / 1000.0 );
		pre = (long) ((endTime - startTime) / 1000.0);
		}
	}
	public static void addSection(){
		w.write("================================================\n");
	}


}
