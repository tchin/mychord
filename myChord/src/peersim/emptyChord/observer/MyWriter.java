package peersim.emptyChord.observer;


import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class MyWriter {
	FileWriter fstream;
	BufferedWriter out;

	public MyWriter(String a) {
		try {
			fstream = new FileWriter(a, true);
			out = new BufferedWriter(fstream);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void write(String a) {
		try {
			out.write(a);
			out.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	public void close() {
		try {
			out.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}	
}
