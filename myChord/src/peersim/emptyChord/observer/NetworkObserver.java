package peersim.emptyChord.observer;

import peersim.config.Configuration;
import peersim.core.Control;
import peersim.core.Network;
import peersim.emptyChord.EmptyChordProtocol;
import peersim.emptyChord.PeersimConfig;
import peersim.emptyChord.ui.Main;
import peersim.myChord.SimpleChordProtocol;

public class NetworkObserver implements Control {
	private final String prefix;
	private final int pid;
	private Main main;
	private final int idLength;
	public NetworkObserver(String prefix) {
		super();
		this.prefix = prefix;
		this.pid = Configuration.getPid(prefix + "." + PeersimConfig.PAR_PROT);
		this.idLength = Configuration.getInt(prefix + "." + PeersimConfig.PAR_IDLENGTH);
		final String[] para = new String[] {idLength+""};

		Runnable run = new Runnable(){

			@Override
			public void run() {
				// TODO Auto-generated method stub
				
				main = new Main();
				main.main(para);					
			}
			
		};
		Thread a = new Thread(run);
		//a.start();
		
		
	}

	@Override
	public boolean execute() {
		// TODO Auto-generated method stub
		int size = Network.size();	
		String chord;
		String successor;
		System.out.println("monitoring");
		
		for (int i = 0; i < size; i++) {
			
			EmptyChordProtocol cp = (EmptyChordProtocol) Network.get(i).getProtocol(pid);
			chord = cp.getChordId().toString();

			if (cp.getSuccessorList()[0]!= null){
				successor = ((EmptyChordProtocol) cp.getSuccessorList()[0].getProtocol(pid)).getChordId().toString();
				
			}else
				successor = "";
			
		}		
		return false;
	}

}
