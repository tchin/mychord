package peersim.emptyChord;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.UnknownHostException;

import peersim.emptyChord.observer.StabilizeObserver;
import peersim.emptyChord.ui.message.AddNodeMessage;
import peersim.emptyChord.ui.message.ClearAllMessage;
import peersim.emptyChord.ui.message.ConnectNodeMessage;
import peersim.emptyChord.ui.message.DeleteNodeMessage;
import peersim.emptyChord.ui.message.RemoveLineMessage;

public class KnockKnockClient {
	public static final String magicNumber = "1234567890";
	public static Socket kkSocket;
	public static ObjectOutputStream out;
	//public static BufferedReader in;

	
	public static void addNode(String server) {

	
	}
	public static void deleteNode(String server) {

	}
	public static void connectNode(String from, String to) {
		StabilizeObserver.stabilize();

	}	
	
	public static void clearAll() {
		StabilizeObserver.writeResult();
	}	
	public static void removeLine(String from) {

	}		
	
}
