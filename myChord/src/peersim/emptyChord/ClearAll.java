package peersim.emptyChord;

import java.math.BigInteger;

import peersim.config.Configuration;
import peersim.core.CommonState;
import peersim.core.Control;
import peersim.core.Network;
import peersim.core.Node;
import peersim.emptyChord.message.NotifyMessage;
import peersim.emptyChord.observer.StabilizeObserver;

public class ClearAll  implements Control{
	private final String prefix;
	private static final String PAR_PROT = "protocol";
	private final int pid;		
	public ClearAll(String prefix) {
		super();
		this.prefix = prefix;
		this.pid = Configuration.getPid(prefix + "." + PAR_PROT);
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean execute() {
		// TODO Auto-generated method stub
//		MyDynamicNetwork.flag = false;
		ChordInitializer.created = false;
		ChordIdGenerator.ids.clear();
		StabilizeObserver.writeResult();
		StabilizeObserver.startTime = 0;
		StabilizeObserver.endTime = 0;
	
		StabilizeObserver.addSection();
		NotifyMessage.count =0 ;
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		KnockKnockClient.clearAll();

		return false;
	}

}
