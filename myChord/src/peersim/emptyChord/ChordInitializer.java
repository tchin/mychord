package peersim.emptyChord;

import java.util.Random;
import peersim.config.Configuration;
import peersim.core.CommonState;
import peersim.core.Network;
import peersim.core.Node;
import peersim.dynamics.NodeInitializer;
import peersim.emptyChord.message.ChordMessageType;
import peersim.emptyChord.message.FindSuccessorMessage;
import peersim.transport.Transport;

//class that is used to initialized node to join the network/
//algorithm used here is randomly contact a node in network and join
public class ChordInitializer implements NodeInitializer {

	private int pid = 0;
	private int fail = 0; // counter for fail to find bootstrap server
	private EmptyChordProtocol currentp;
	public static boolean created = false;
	public ChordInitializer(String prefix) {
		pid = Configuration.getPid(prefix + "." + PeersimConfig.PAR_PROT);
	}

	// when initialize is call , the node n is already
	// existed
	public void initialize(Node current) {
		currentp = (EmptyChordProtocol) current.getProtocol(pid);
		currentp.setNode(current);
		join(current);
	}

	public void join(final Node current) {

		// TODO Auto-generated method stub
		Node bootStrapNode = getBootStrapingNode();
		// Sending message looking for successor through BootStrapping
		// Node
		if (bootStrapNode != null) {
	
			Transport t = (Transport) current
					.getProtocol(((EmptyChordProtocol) current.getProtocol(pid))
							.getTranspotProtocol());
			FindSuccessorMessage message = new FindSuccessorMessage(current,
					bootStrapNode,
					((EmptyChordProtocol) current.getProtocol(pid))
							.getChordId(), ChordMessageType.REQUESTJOIN);
			t.send(current, bootStrapNode, message, pid);

		} else {
			if (created == false){
			((EmptyChordProtocol) current.getProtocol(pid)).create(current);
			KnockKnockClient.addNode(((EmptyChordProtocol) current.getProtocol(pid)).getChordId().toString());

			fail++;
			created = true;
			}
			
		}
	}

	public Node getBootStrapingNode() {
		String a;
		Random generator = new Random(CommonState.r.nextLong());
		int numberOfTry = 0;
		Node other;
		do {
			if (Network.size() <= 0)
				return null;
			other = Network.get(generator.nextInt(Network.size()));
			a = ((EmptyChordProtocol) other.getProtocol(pid)).getChordId()
					.toString();
			numberOfTry++;
			if (numberOfTry > 10) {
				return Network.get(0);
				//break;
			}
		} while (other == null
				|| other.isUp() == false
				|| ((EmptyChordProtocol) other.getProtocol(pid)).joinState == false);
		return other;
	}

}
