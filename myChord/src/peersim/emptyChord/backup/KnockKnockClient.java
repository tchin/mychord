package peersim.emptyChord.backup;
/*
 * Copyright (c) 1995, 2008, Oracle and/or its affiliates. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   - Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   - Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *
 *   - Neither the name of Oracle or the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

import java.io.*;
import java.net.*;

import peersim.emptyChord.observer.StabilizeObserver;
import peersim.emptyChord.ui.message.AddNodeMessage;
import peersim.emptyChord.ui.message.ClearAllMessage;
import peersim.emptyChord.ui.message.ConnectNodeMessage;
import peersim.emptyChord.ui.message.DeleteNodeMessage;
import peersim.emptyChord.ui.message.RemoveLineMessage;

public class KnockKnockClient {
	public static final String magicNumber = "1234567890";
	public static Socket kkSocket;
	public static ObjectOutputStream out;
	//public static BufferedReader in;
	static {
		try {
			kkSocket = new Socket("localhost", 5555);
			kkSocket.setReuseAddress(true);
			//kkSocket.setReceiveBufferSize(Integer.MAX_VALUE);
			kkSocket.setSendBufferSize(Integer.MAX_VALUE);
			out = new ObjectOutputStream(kkSocket.getOutputStream());
	//		in = new BufferedReader(new InputStreamReader(
	//				kkSocket.getInputStream()));
		} catch (UnknownHostException e) {
			System.err.println("Don't know about host: " + "localhost");
			System.exit(1);
		} catch (IOException e) {
			System.err.println("1Couldn't get I/O for the connection to: "
					+ "localhost");
			e.printStackTrace();
			System.exit(1);
			
		}	
	}
	
	public static void addNode(String server) {

		AddNodeMessage mesg = new AddNodeMessage(server);
	    ByteArrayOutputStream baos = new ByteArrayOutputStream();
	    ObjectOutputStream oos ;


	    try {
	    	//oos = new ObjectOutputStream(baos);
			//oos.writeObject(mesg);
			//oos.flush();
			//oos.close();
			out.writeObject(mesg);
			out.flush();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (Exception e){
			e.printStackTrace();
		}
	}
	public static void deleteNode(String server) {

		DeleteNodeMessage mesg = new DeleteNodeMessage(server);
		
	    ByteArrayOutputStream baos = new ByteArrayOutputStream();
	    ObjectOutputStream oos ;


	    try {
	    	//oos = new ObjectOutputStream(baos);
			//oos.writeObject(mesg);
			//oos.flush();
			//oos.close();
	    	out.writeObject(mesg);
			out.flush();
			
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}catch (Exception e){
			e.printStackTrace();
		}
	}
	public static void connectNode(String from, String to) {
		StabilizeObserver.stabilize();
		ConnectNodeMessage mesg = new ConnectNodeMessage(from, to);
		
	    ByteArrayOutputStream baos = new ByteArrayOutputStream();
	    ObjectOutputStream oos ;


	    try {
	    	//oos = new ObjectOutputStream(baos);
			//oos.writeObject(mesg);
			//oos.flush();
			//oos.close();
	    	out.writeObject(mesg);
			out.flush();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}catch (Exception e){
			e.printStackTrace();
		}
	}	
	
	public static void clearAll() {
		ClearAllMessage mesg = new ClearAllMessage();
		
	    ByteArrayOutputStream baos = new ByteArrayOutputStream();
	    ObjectOutputStream oos ;


	    try {
	    	//oos = new ObjectOutputStream(baos);
			//oos.writeObject(mesg);
			//oos.flush();
			//oos.close();
	    	out.writeObject(mesg);
			out.flush();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}catch (Exception e){
			e.printStackTrace();
		}
	}	
	public static void removeLine(String from) {
		RemoveLineMessage mesg = new RemoveLineMessage(from);
		
	    ByteArrayOutputStream baos = new ByteArrayOutputStream();
	    ObjectOutputStream oos ;


	    try {
	    	//oos = new ObjectOutputStream(baos);
			//oos.writeObject(mesg);
			//oos.flush();
			//oos.close();
	    	out.writeObject(mesg);
			out.flush();
			
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}catch (Exception e){
			e.printStackTrace();
		}
	}		
	
}