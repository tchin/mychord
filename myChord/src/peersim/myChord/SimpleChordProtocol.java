package peersim.myChord;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigInteger;

import peersim.config.Configuration;
import peersim.core.CommonState;
import peersim.core.Network;
import peersim.core.Node;
import peersim.edsim.EDProtocol;
import peersim.transport.Transport;

public class SimpleChordProtocol implements EDProtocol {
	public static boolean debug = true;
	private Node node;
	// Misc variable
	public String prefix;
	static MyWriter w = new MyWriter("out2.txt");

	public Parameters p;
	public boolean joinState = false;
	// Chord information
	public BigInteger chordId;
	public Node[] fingerTable;
	public Node[] successorList;
	public int succLSize; // succlist size? not initialize how come

	public int m;// fingerTableSize
	private int currentNode = 0;// index of current Node

	public Node predecessor;
	// Configuration parameter
	private static final String PAR_TRANSPORT = "transport";

	// debug information
	public int varSuccList = 0;

	// fix finger table information
	private int next = 0;

	// stabilization counter
	public int stabilizations = 0;

	public int fails = 0;

	// lookup message variable section
	private int[] lookupMessage;
	public int index; // counter

	/**
	 * 
	 */

	public SimpleChordProtocol(String prefix) {
		this.prefix = prefix;
		lookupMessage = new int[1];
		lookupMessage[0] = 0;
		p = new Parameters();
		p.tid = Configuration.getPid(prefix + "." + PAR_TRANSPORT);
	}

	public void setNode(Node n) {
		this.node = n;
	}

	public Node getNode() {
		return node;
	}

	@Override
	public void processEvent(Node node, int pid, Object event) {
		// TODO Auto-generated method stub
		// processare le richieste a seconda della routing table del nodo

		Node q = node;
		if (q.getProtocol(pid) instanceof SimpleChordProtocol){
			p.pid = pid;
			currentNode = node.getIndex();		
		}
		if (event.getClass() == LookUpMessage.class) {
		
			LookUpMessage message = (LookUpMessage) event;
			message.increaseHopCounter();
			BigInteger target = message.getTarget();
			Transport t = (Transport) node.getProtocol(p.tid);
			Node n = message.getSender();
			if (target == ((SimpleChordProtocol) node.getProtocol(pid)).chordId) {
				t.send(node, n, new FinalMessage(message.getHopCounter()), pid);
			}
			if (target != ((SimpleChordProtocol) node.getProtocol(pid)).chordId) {
				// routing
				
				Node dest = this.find_successor(target);
				if (dest == null ){
					fails++;
				} else {
					dest = ((SimpleChordProtocol)dest.getProtocol(pid)).predecessor;
					if (dest.isUp() && ((SimpleChordProtocol)dest.getProtocol(pid)).chordId.compareTo(target)==0){
						t.send(message.getSender(), dest, message, pid);
//					System.out.println("sending from" + chordId + "to "+ target +" through "
//							+ ((SimpleChordProtocol)dest.getProtocol(pid)).chordId);
					} else {
						fails++;
					}
				}
				

				/*
				if (dest == null) {
					if (this.successorList[0] != null
							&& this.successorList[0].isUp()) {
						dest = successorList[0];
						if ((target.compareTo(((SimpleChordProtocol) dest
								.getProtocol(p.pid)).chordId) < 0)) {
							//System.out.println("Faile");
							fails++;
						} else {
							t.send(message.getSender(), dest, message, pid);
						}
					} else {
						this.updateSuccessor();
						if (this.successorList[0] != null
								&& this.successorList[0].isUp()) {
							
							dest = successorList[0];
							if ((target.compareTo(((SimpleChordProtocol) dest
									.getProtocol(p.pid)).chordId) < 0)) {
								//System.out.println("Faile");
								fails++;
							} else {
								t.send(message.getSender(), dest, message, pid);
								System.out.println("seding");
							}

						} else {
							fails++;
						}
					}

				}
				*/

			}

		}
		if (event.getClass() == FinalMessage.class) {			
			FinalMessage message = (FinalMessage) event;
			lookupMessage = new int[index + 1];
			lookupMessage[index] = message.getHopCounter();
			index++;
		}
		

	}


	public Object clone() {
		SimpleChordProtocol cp = new SimpleChordProtocol(prefix);
		String val = BigInteger.ZERO.toString();
		cp.chordId = new BigInteger(val);
		cp.m = this.m;
		cp.fingerTable = new Node[m];
		cp.succLSize = succLSize;
		cp.successorList = new Node[succLSize];
		cp.currentNode = 0;
		return cp;
	}

	// this successor is not recursive
	// this methoid is to find succesor for a given id
	// it would search through the finger table and find a subitable id
	// it would return null if all the finger table is off line
	// it wourld return the cloest possible node
	public Node find_successor(BigInteger id) {
		if (this.node.isUp()) {

			try {
				if (id == null) {
					System.out.println("key error");
				}
				if (debug)
					w.write("" + this.chordId + "with id =" + id
							+ "finding successor \n");
				if (this.successorList[0] != null)
					if (this.successorList[0].isUp() == false) {
						this.successorList[0] = null;
					}

				if (id.compareTo(chordId) == 0) {
					/*
					 * w.write("found- " + ((SimpleChordProtocol)
					 * successorList[0] .getProtocol(p.pid)).chordId + "\n");
					 * return this.successorList[0];
					 */

					if (successorList[0] != null && successorList[0].isUp()) {
						if (debug)
							w.write("found- "
									+ ((SimpleChordProtocol) successorList[0]
											.getProtocol(p.pid)).chordId + "\n");
						return successorList[0];
					} else {
						this.closest_preceding_node(id);
						Node tmp = closest_preceding_node(id);
						if (tmp == null) {
							this.stabilize(this.node);
							this.stabilizations++;
							this.fixFingers();
							// System.out.println("system failed");
							return find_successor(id);

						} else {
							w.write("foun%%- "
									+ ((SimpleChordProtocol) tmp
											.getProtocol(p.pid)).chordId + "\n");
							return tmp;
						}
					}

				}
				int counter =0;
				while (successorList[0] == null
						|| successorList[0].isUp() == false) {
					System.out.println("updating succesr");
					counter++;
					this.updateSuccessor();
					if (counter > 12){
						return node;
					}
				}

				if (successorList[0] != null) {
					if (debug)
						w.write("checking "
								+ id
								+ "in range with "
								+ chordId
								+ " "
								+ ((SimpleChordProtocol) successorList[0]
										.getProtocol(p.pid)).chordId + "\n");
					if (idInab(id, this.chordId,
							((SimpleChordProtocol) successorList[0]
									.getProtocol(p.pid)).chordId)) {
						if (debug)
							w.write("found* "
									+ ((SimpleChordProtocol) successorList[0]
											.getProtocol(p.pid)).chordId + "\n");
						// printAllNode();
						return successorList[0];
					} else {
						Node tmp = closest_preceding_node(id);
						if (tmp == null) {
							tmp = successorList[0];
						//	this.updateSuccessor();
//							System.out.println("fuuuuuuuuuuuuuuuuuu"
//									+ tmp.isUp());

						}
						// System.out.println("^^^^^^^^^^^^^^^^^" + tmp.isUp());
						// w.write("not in range next node to find "
						// + ((SimpleChordProtocol)
						// tmp.getProtocol(p.pid)).chordId
						// + "\n");
						return (((SimpleChordProtocol) tmp.getProtocol(p.pid))
								.find_successor(id));
						// printAllNode();
						// return tmp;
					}
				} else {
					this.successorList[0] = Network.get(0);
				}
				/*
				 * else {
				 * 
				 * Node tmp = closest_preceding_node(id);
				 * 
				 * // w.write("not in range next node to find " // +
				 * ((SimpleChordProtocol) tmp.getProtocol(p.pid)).chordId // +
				 * "\n"); if (tmp == null){ Node n = Network.get(0); tmp = n; }
				 * return (((SimpleChordProtocol) tmp.getProtocol(p.pid))
				 * .find_successor(id)); }
				 */

			} catch (Exception e) {
				e.printStackTrace();
			}
			/*
			 * if (successorList[0] == null){ Node n = Network.get(0);
			 * SimpleChordProtocol cp =(SimpleChordProtocol)
			 * n.getProtocol(p.pid); return (cp.find_successor(id));
			 * 
			 * }
			 */
			if (debug)
				w.write("found /"
						+ ((SimpleChordProtocol) successorList[0]
								.getProtocol(p.pid)).chordId);
			// printAllNode();
		} else {
			return null;
		}
		return successorList[0];
	}

	private void printAllNode() {
		w.write("\nBegin\n");
		for (int i = 0; i < 1000; i++) {

			try {
				Node node = (Node) Network.get(i);
				if (node != null) {
					SimpleChordProtocol cp = ((SimpleChordProtocol) node
							.getProtocol(p.pid));
					w.write(node + "chorid id=" + cp.chordId + " stablilize="
							+ cp.joinState + "\n");
					w.write("\n "
							+ cp.chordId
							+ "link to successor "
							+ ((SimpleChordProtocol) cp.successorList[0]
									.getProtocol(p.pid)).chordId + "\n");
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block

			}
		}
		w.write("\nEnd\n");
	}

	// i-1 because i start with m as size of the finger Table

	private Node closest_preceding_node(BigInteger id) {
		if (this.node.isUp()) {
			if (debug)
				w.write("\n==============Begin===========\n");
//			this.printFingers();
			if (chordId.compareTo(id) == 1
					&& chordId.compareTo(BigInteger.ZERO) == -1) {
				if (fingerTable[m - 1] == null
						|| fingerTable[m - 1].isUp() == false
						|| ((SimpleChordProtocol) fingerTable[m - 1]
								.getProtocol(p.pid)).joinState == false) {
					if (debug)
						w.write("return null\n");
					return null;
				} else {
					if (debug)
						w.write("return top\n");
					return fingerTable[m - 1];
				}
			}
			Node current = fingerTable[m - 1];
			for (int i = m; i > 0; i--) {
				try {
					current = fingerTable[i - 1];
					// discarding offline node or joing node
					// if (fingerTable[i - 1] != null)
					// w.write(i + "\n Begin finding closed note with id" + id +
					// "with" +
					// ((SimpleChordProtocol)fingerTable[i-1].getProtocol(p.pid)).chordId
					// +"\n");
					if (fingerTable[i - 1] == null
							|| fingerTable[i - 1].isUp() == false
							|| ((SimpleChordProtocol) fingerTable[i - 1]
									.getProtocol(p.pid)).joinState == false) {
						// if (fingerTable[i - 1] != null)
						// w.write(((SimpleChordProtocol)fingerTable[i-1].getProtocol(p.pid)).chordId
						// + " failure\n");
						continue;
					}
					if (fingerTable[i - 1] != null) {
						if ((SimpleChordProtocol) (fingerTable[i - 1]
								.getProtocol(p.pid)) == null) {
							if (debug)
								w.write("uuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuu");
						}
					}
					BigInteger fingerId = ((SimpleChordProtocol) (fingerTable[i - 1]
							.getProtocol(p.pid))).chordId;
					if (debug)
						w.write("\nchecking " + fingerId + " in range of"
								+ this.chordId + " to " + id + "\n");
					if ((idInab2(fingerId, this.chordId, id))
							|| (id.compareTo(fingerId) == 0)) {
						if (debug)
							w.write("\nchecking result yes" + "and returnning"
									+ fingerId + "\n");
						BigInteger fingerId2 = ((SimpleChordProtocol) (fingerTable[i - 1]
								.getProtocol(p.pid))).chordId;
						if (debug)
							w.write("actucal return" + fingerId2 + "\n");
						return fingerTable[i - 1];
					}

				} catch (Exception e) {

					e.printStackTrace();
				} finally {
					if (debug)
						w.write("\n==============End===========\n");
				}
			}
			if (debug)
				w.write("return original\n");

			// if (fingerTable[m - 1] == null)

			// if (a.compareTo(b) == 0)
			// return this.successorList[0];
		} else {
			return null;
		}
		return null;
		// return
		// ((SimpleChordProtocol)Network.get(0).getProtocol(p.pid)).find_successor(id);
	}

	private boolean idInab3(BigInteger id, BigInteger a, BigInteger b) {
		// if ((a.compareTo(id) == -1) && (id.compareTo(b) == -1)) {
		// return true;
		// }
		// id is in the range of a b where a > b
		if (a.compareTo(b) == 1) {

			if (id.compareTo(a) == 1 && id.compareTo(b) == 1) {
				return true;
			}
			
			if (id.compareTo(a) == -1 && id.compareTo(b) == -1) {
				return true;
			}
			if (id.compareTo(a) == -1 && id.compareTo(b) == 1)
				return true;
			// if (id.compareTo(b) == -1 && a.compareTo(id) == 1 )
			// return true;
			// if(id.compareTo(b) == 1 && a.compareTo(id) == -1)
			// return true;
			// id is in the range of a b where a < b
		} else {
			// id is in the range of a and b
			if (id.compareTo(b) == -1 && id.compareTo(a) == 1)
				return true;
			
			//
			// if (id.compareTo(b) == -1 && a.compareTo(id) == 1)
			// return true;
		}

		// if there is only 1 node in chord
		if (a.compareTo(b) == 0) {
			return true;
		}
		if (a.compareTo(id) == 0) {
			return true;
		}
		// if there is only 1 node in chord
		// if (b.compareTo(id) == 0) {
		// return false;
		// }
		return false;

	}

	// method to compare wheter the id is in between value a and b
	private boolean idInab(BigInteger id, BigInteger a, BigInteger b) {

		// if ((a.compareTo(id) == -1) && (id.compareTo(b) == -1)) {
		// return true;
		// }
		// id is in the range of a b where a > b
		if (a.compareTo(b) == 1) {

			if (id.compareTo(a) == 1 && id.compareTo(b) == 1) {
				return true;
			}
			if (id.compareTo(a) == -1 && id.compareTo(b) == -1) {
				return true;
			}
			// if (id.compareTo(b) == -1 && a.compareTo(id) == 1 )
			// return true;
			// if(id.compareTo(b) == 1 && a.compareTo(id) == -1)
			// return true;
			// id is in the range of a b where a < b
		} else {
			// id is in the range of a and b
			if (id.compareTo(b) == -1 && id.compareTo(a) == 1)
				return true;
			//
			// if (id.compareTo(b) == -1 && a.compareTo(id) == 1)
			// return true;
		}

		// if there is only 1 node in chord
		if (a.compareTo(b) == 0) {
			return true;
		}
		if (a.compareTo(id) == 0) {
			return true;
		}
		// if there is only 1 node in chord
		// if (b.compareTo(id) == 0) {
		// return false;
		// }
		return false;
	}

	private boolean idInab2(BigInteger id, BigInteger a, BigInteger b) {

		// if ((a.compareTo(id) == -1) && (id.compareTo(b) == -1)) {
		// return true;
		// }
		// id is in the range of a b where a > b
		if (a.compareTo(b) == 1) {
			// if (id.compareTo(a) == -1 && id.compareTo(b) == 1) {
			// return true;
			// }
			// if (id.compareTo(b) == -1)
			// return true;
			// if (id.compareTo(b) == -1 && a.compareTo(id) == 1 )
			// return true;
			// if(id.compareTo(b) == 1 && a.compareTo(id) == -1)
			// return true;
			// id is in the range of a b where a < b
		} else {
			// id is in the range of a and b
			if (id.compareTo(b) == -1 && id.compareTo(a) == 1)
				return true;

			//
			// if (id.compareTo(b) == -1 && a.compareTo(id) == 1)
			// return true;
		}

		// if there is only 1 node in chord
		if (a.compareTo(b) == 0) {
			return true;
		}

		// if there is only 1 node in chord
		if (b.compareTo(id) == 0) {
			return true;
		}

		return false;
	}

	public void printFingers() {
		for (int i = fingerTable.length - 1; i > 0; i--) {
			if (fingerTable[i] == null) {
				if (debug)
					w.write("Finger " + i + " is null");
				continue;
			}

			if (debug)
				w.write("Finger["
						+ i
						+ "] = "
						+ fingerTable[i].getIndex()
						+ " chordId "
						+ ((SimpleChordProtocol) fingerTable[i]
								.getProtocol(p.pid)).chordId + "\n");
		}
	}

	public void fixFingers() {
		if (this.node.isUp()) {
			// Reset back next if more then the length of bit
			if (next >= m)
				next = 0;
			// if next finger is initialized skip fix and move next pointer to
			// next
			// finger entry
			if (fingerTable[next] != null && fingerTable[next].isUp()) {
				next++;
				return;
			}
			// varaible ussage
			BigInteger base;

			// calculate base according to next number
			if (next == 0)
				base = BigInteger.ONE;
			else {
				base = BigInteger.valueOf(2);
				for (int exp = 1; exp < next; exp++) {
					base = base.multiply(BigInteger.valueOf(2));
				}
			}

			// pot represent entry to be fix
			BigInteger pot = this.chordId.add(base);

			do {
				fingerTable[next] = find_successor(pot);
				pot = pot.subtract(BigInteger.ONE);
				// ((SimpleChordProtocol) successorList[0].getProtocol(p.pid))
				// .fixFingers();
			} while (fingerTable[next] == null
					|| fingerTable[next].isUp() == false);
			next++;
		}
	}

	public void stabilize(Node myNode) {
		try {
			if (this.node.isUp()) {
				if (successorList[0] == null
						|| successorList[0].isUp() == false) {
					// need algorithm to find back online successor node
//					System.out.println("update");
					updateSuccessor();
				}

				// node here is the predecessor of the successor of this node
				Node node = ((SimpleChordProtocol) successorList[0]
						.getProtocol(p.pid)).predecessor;
				if (node != null && node.isUp() == true) {
					// no need to stalilize cause successor still point back to
					// this
					// node
					if (this.chordId.compareTo(((SimpleChordProtocol) node
							.getProtocol(p.pid)).chordId)==0)
						return;
					BigInteger remoteID = ((SimpleChordProtocol) node
							.getProtocol(p.pid)).chordId;
//					System.out.println("remoteid"
//							+ remoteID
//							+ "="
//							+ chordId
//							+ "-"
//							+ ((SimpleChordProtocol) successorList[0]
//									.getProtocol(p.pid)).chordId
//							+ "$$"
//							+ idInab3(remoteID, chordId,
//									((SimpleChordProtocol) successorList[0]
//											.getProtocol(p.pid)).chordId));
					if (idInab3(remoteID, chordId,
							((SimpleChordProtocol) successorList[0]
									.getProtocol(p.pid)).chordId)) {
//						System.out.println("change");
						successorList[0] = node;
					}

				}
				((SimpleChordProtocol) successorList[0].getProtocol(p.pid))
						.notify(myNode);
			}
		} catch (Exception e1) {
			e1.printStackTrace();

		}
	}
	public void reJoin (){

		Node n = Network.get(0);
		Node successor ;

		SimpleChordProtocol cpRemote = (SimpleChordProtocol) n.getProtocol(p.pid);		

		

		successor = cpRemote.find_successor(chordId);


//		w.write("rejoin" + chordId + " to successor " + ((SimpleChordProtocol) successor.getProtocol(p.pid)).chordId +"\n");

	
		
		varSuccList = 0;

		successorList = new Node[succLSize];
		if (successor == null){
			//if (this.chordId.compareTo(cpRemote.chordId) == 1){
				successorList[0] = n;
			//}
		} else {
		successorList[0] = successor;
		}
		fingerTable = new Node[m];
		System.out.println("454364554==" +successor.getIndex());

		try {
			if (successorList[0]!=null ||successorList[0].isUp() == true)
			updateSuccessorList();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		stabilizations++;
		stabilize(node);
		fixFingers();	
	}
	
	// need algorithm to find back online successor node
	public void updateSuccessor() {
		if (this.node.isUp()) {
			boolean searching = true;
			int counter = 0;
			while (searching && this.node.isUp()) {
//				System.out.println(counter);
				counter++;
				if (counter > 12){
					//need to join back the network bug
					System.out.println("DFSDFDSFSD" + varSuccList);
					System.out.println("DFSDFDSFSD" +chordId);
					if (node.getIndex() != 0){
						reJoin();
						if (this.successorList[0] == null){
							this.successorList[0] = Network.get(0);
						}
					}
					
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				if (counter > 200){
					//need to join back the network bug
					break;
				}
				try {
					if (varSuccList >= succLSize - 1) {
						varSuccList = 0;
					}
					Node node = successorList[varSuccList];
					varSuccList++;
					successorList[0] = node;
					if (successorList[0] == null
							|| successorList[0].isUp() == false) {

						// searching = false;

					} else {
						updateSuccessorList();
						searching = false;
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			if (successorList[0] == null || successorList[0].isUp() == false)
				System.out.println("successor not found system error at node"
						+ this.chordId);
		}

	}

	public void updateSuccessorList() throws Exception {
		try {
			while (successorList[0] == null || successorList[0].isUp() == false) {
				updateSuccessor();
			}
			System.arraycopy(
					((SimpleChordProtocol) successorList[0].getProtocol(p.pid)).successorList,
					0, successorList, 1, succLSize - 2);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void notify(Node node) throws Exception {
		if (this.node.isUp()) {
			BigInteger nodeId = ((SimpleChordProtocol) node.getProtocol(p.pid)).chordId;
			if (predecessor != null) {
//				System.out
//						.println("notifying "
//								+ nodeId
//								+ "**"
//								+ ((SimpleChordProtocol) predecessor
//										.getProtocol(p.pid)).chordId + "**"
//								+ chordId);
//				System.out
//						.println(idInab3(nodeId,
//								((SimpleChordProtocol) predecessor
//										.getProtocol(p.pid)).chordId,
//								this.chordId));
			}
			if ((predecessor == null)
					|| (idInab3(nodeId,
							((SimpleChordProtocol) predecessor
									.getProtocol(p.pid)).chordId, this.chordId))) {
				predecessor = node;

			}
		}
	}

	public void checkPredecessor() {
		if (this.node.isUp()) {
			if (this.predecessor != null && this.predecessor.isUp() == false) {
				this.predecessor = null;
			}
		}
	}

	// lookup message implementation
	public void emptyLookupMessage() {
		index = 0;
		this.lookupMessage = new int[0];
	}

	public int[] getLookupMessage() {
		return lookupMessage;
	}
}
