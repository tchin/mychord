package peersim.myChord;

public class FileFinalMessage {
	private int hopCounter = 0;
	private FileDownloader downloader;
	public FileFinalMessage(int hopCounter,  FileDownloader downloader) {
		this.hopCounter = hopCounter;
		this.downloader = downloader;
	}

	public int getHopCounter() {
		return hopCounter;
	}
	public FileDownloader getDownloader(){
		return downloader;
	}
	
}
