package peersim.myChord;

import java.math.BigInteger;

import peersim.config.Configuration;
import peersim.core.CommonState;
import peersim.core.Control;
import peersim.core.Network;
import peersim.core.Node;
//This ChordCreation is base on instance creation of working node.
//and is not started from 0;
public class ChordCreation implements Control {
	private int pid = 0;

	public static int idLength = 0;

	int successorLsize = 0;

	// Configuration parameter
	private static final String PAR_IDLENGTH = "idLength";
	private static final String PAR_PROT = "protocol";
	private static final String PAR_SUCCSIZE = "succListSize";

	public ChordCreation(String prefix) {
		pid = Configuration.getPid(prefix + "." + PAR_PROT);
		idLength = Configuration.getInt(prefix + "." + PAR_IDLENGTH);
		successorLsize = Configuration.getInt(prefix + "." + PAR_SUCCSIZE);
	}

	@Override
	public boolean execute() {
		// TODO Auto-generated method stub

		for (int i = 0; i < Network.size(); i++) {
			Node node = (Node) Network.get(i);
			SimpleChordProtocol cp = (SimpleChordProtocol) node
					.getProtocol(pid);
			cp.m = idLength;// set fingle table size with idLength
			cp.succLSize = successorLsize;
			cp.varSuccList = 0;
			cp.p.pid = pid;
			cp.chordId = new BigInteger(idLength, CommonState.r);
			
			boolean detect = false;
			do {
				detect = false;
				for (int j = 0 ; j < i;j++){
					
					Node node2 = (Node) Network.get(j);
					SimpleChordProtocol cp2 = (SimpleChordProtocol) node2
							.getProtocol(pid);
					if (cp2.chordId.compareTo(cp.chordId) == 0){
						detect = true;
						cp.chordId = new BigInteger(idLength, CommonState.r);
						break;
					}
					
				}
			} while (detect == true);
			
			cp.fingerTable = new Node[idLength];
			cp.successorList = new Node[successorLsize];
			cp.setNode(node);
//			System.out.println(node + " " + cp.chordId);
		}
		
		NodeComparator nc = new NodeComparator(pid);
		Network.sort(nc);
		createFingerTable();

		return false;
	}

	public void createFingerTable() {
		BigInteger idFirst = ((SimpleChordProtocol) Network.get(0).getProtocol(
				pid)).chordId;
		String testFirst = idFirst.toString();
		BigInteger idLast = ((SimpleChordProtocol) Network.get(
				Network.size() - 1).getProtocol(pid)).chordId;
		String testLast = idLast.toString();
		String testIds;
		//initialize succeesor list
//		System.out.println("initialize sussessor list");
		for (int i = 0; i < Network.size(); i++) {
			Node node = (Node) Network.get(i);
			SimpleChordProtocol cp = (SimpleChordProtocol) node
					.getProtocol(pid);
			for (int a = 0; a < successorLsize; a++) {
				cp.successorList[a] = Network.get((a + i + 1) % Network.size());
				BigInteger q = ((SimpleChordProtocol)cp.successorList[a].getProtocol(pid)).chordId;
				testIds = q.toString();
//				System.out.println(i+ "..." +a + "_------" + cp.chordId +"||||"+ q  );
			}
//			System.out.println("ENd sussessor list");
			if (i > 0)
				cp.predecessor = (Node) Network.get(i - 1);
			else
				cp.predecessor = (Node) Network.get(Network.size() - 1);
		}
		
		//initialize finger table
		for (int i = 0; i < Network.size(); i++) {
			Node node = (Node) Network.get(i);
			SimpleChordProtocol cp = (SimpleChordProtocol) node
					.getProtocol(pid);
			BigInteger two = BigInteger.ONE.add(BigInteger.ONE);
			BigInteger base;
			String testbase;
			String testPot;
			for (int j = 0; j < idLength; j++) {
				base = two.pow(j);
				testbase = base.toString();
				BigInteger pot = cp.chordId.add(base);
				testPot = pot.toString();
//					System.out.println(i+ "..." +j + "_------" + cp.chordId +"||||"+ pot + "assing"
//							+ ((SimpleChordProtocol)myFindId(pot, node).getProtocol(pid)).chordId);
				
				Node n = myFindId(pot, node);
				BigInteger id = ((SimpleChordProtocol)n.getProtocol(pid)).chordId;
				String testFID = id.toString();
				 cp.fingerTable[j] =  n;
				
			}

			cp.joinState = true;
		}
	}

	public Node myFindId(BigInteger id, Node current) {
		SimpleChordProtocol cp = (SimpleChordProtocol) current.getProtocol(pid);
		Node succ = cp.successorList[0];
		SimpleChordProtocol cp2 = (SimpleChordProtocol) succ.getProtocol(pid);
		if (idInab(id, cp.chordId, cp2.chordId)) {
			return succ;
		} else {
			return myFindId(id, succ);
		}

	}

	private boolean idInab(BigInteger id, BigInteger a, BigInteger b) {

		// if ((a.compareTo(id) == -1) && (id.compareTo(b) == -1)) {
		// return true;
		// }
		// id is in the range of a b where a > b
		if (a.compareTo(b) == 1) {
			if (id.compareTo(a) == 1 && id.compareTo(b) == 1) {
				return true;
			}
			if (id.compareTo(a) == -1 && id.compareTo(b) == -1) {
				return true;
			}
			// if (id.compareTo(b) == -1 && a.compareTo(id) == 1 )
			// return true;
			// if(id.compareTo(b) == 1 && a.compareTo(id) == -1)
			// return true;
			// id is in the range of a b where a < b
		} else {
			// id is in the range of a and b
			if (id.compareTo(b) == -1 && id.compareTo(a) == 1)
				return true;
			//
			// if (id.compareTo(b) == -1 && a.compareTo(id) == 1)
			// return true;
		}

		// if there is only 1 node in chord
		if (a.compareTo(b) == 0 && id.compareTo(a) == 0) {
			return true;
		}

		// if there is only 1 node in chord
		if (b.compareTo(id) == 0) {
			return true;
		}
		return false;
	}

	public Node findId(BigInteger id, int nodeOne, int nodeTwo) {
		if (nodeOne >= (nodeTwo - 1))
			return Network.get(nodeOne);

		// half of the machine number
		int middle = (nodeOne + nodeTwo) / 2;
		System.out.println(Network.size() - 1);
		if (((middle) >= Network.size() - 1))
			System.out.print("ERROR: Middle is bigger than Network.size");
		// possible only when no node or 1 node in the network
		if (((middle) <= 0))
			return Network.get(0);
		try {
			// get the middle machine chord id
			BigInteger newId = ((SimpleChordProtocol) ((Node) Network
					.get(middle)).getProtocol(pid)).chordId;
			BigInteger lowId;
			// get the lower bound node chord id
			if (middle > 0)
				lowId = ((SimpleChordProtocol) ((Node) Network.get(middle - 1))
						.getProtocol(pid)).chordId;
			else
				lowId = newId;
			// get higher bound node chord id
			BigInteger highId = ((SimpleChordProtocol) ((Node) Network
					.get(middle + 1)).getProtocol(pid)).chordId;
			// if the chord id is same as the middle chord node or it is greater
			// then middle chord and less then node at upper bound of the middle
			// return middle chord
			if (id.compareTo(newId) == 0
					|| ((id.compareTo(newId) == 1) && (id.compareTo(highId) == -1))) {
				return Network.get(middle);
			}
			// if the chord id that want t find is less then middle and greater
			// then lower bound of middle node
			if ((id.compareTo(newId) == -1) && (id.compareTo(lowId) == 1)) {
				// return lower boun of the middle node
				if (middle > 0)
					return Network.get(middle - 1);
				else
					// if no more range to find node at possition 0 would be the
					// node to be return
					return Network.get(0);
			}
			// if id want to search is less then middle node then call recursive
			// with node one to midle
			if (id.compareTo(newId) == -1) {
				return findId(id, nodeOne, middle);
			} else if (id.compareTo(newId) == 1) {
				return findId(id, middle, nodeTwo);
			}
			return null;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

}
