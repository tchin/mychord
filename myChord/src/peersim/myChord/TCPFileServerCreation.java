package peersim.myChord;

import java.math.BigInteger;
import java.util.Random;

import peersim.config.Configuration;
import peersim.core.CommonState;
import peersim.core.Control;
import peersim.core.Network;
import peersim.core.Node;

public class TCPFileServerCreation implements Control {

	int fileSize;

	FileDownloader downloader;
	int bandwidth;
	// Configuration parameter
	private static final String NUMCLIENT = "num";

	private static final String FILESIZE = "fileSize";
	private static final String BANDWIDTH = "bandwidth";
	
	public TCPFileServerCreation(String prefix) {
	


		fileSize = Configuration.getInt(prefix + "." + FILESIZE);
		bandwidth = Configuration.getInt(prefix + "." + BANDWIDTH);
	}	
	
	@Override
	public boolean execute() {
		downloader= new FileDownloader(fileSize, bandwidth );
	
			downloader.increaseStep(0.4);
		

		

		
		return false;
	}	
}
