package peersim.myChord;

import java.math.BigInteger;

import peersim.core.Node;

public class FileMessage implements ChordMessage {
	private Node sender;

	private BigInteger targetId;

	private int hopCounter = -1;
	
	private FileDownloader downloader;

	public FileMessage(Node sender, BigInteger targetId, FileDownloader downloader) {
		this.sender = sender;
		this.targetId = targetId;
		this.downloader = downloader;
	}

	public FileDownloader getDownloader(){
		return downloader;
	}
	public void increaseHopCounter() {
		hopCounter++;
	}

	/**
	 * @return the senderId
	 */
	public Node getSender() {
		return sender;
	}

	/**
	 * @return the target
	 */
	public BigInteger getTarget() {
		return targetId;
	}

	/**
	 * @return the hopCounter
	 */
	public int getHopCounter() {
		return hopCounter;
	}

}
