package peersim.myChord;

import peersim.config.Configuration;
import peersim.core.CommonState;
import peersim.core.Control;
import peersim.core.Network;
import peersim.core.Node;
import peersim.edsim.EDSimulator;

public class FileDownloaderGenerator implements Control {

	private static final String PAR_PROT = "protocol";
	private static final String PAR_FILEPROT = "fileProtocol";
	private static final String PAR_BAND = "bandwidth";
	private final int pid;
	private final int fid;
	private final int bandwidth;
	public FileDownloader  downloader;
	public boolean run = false;
	int count=0;
	/**
	 * 
	 */
	public FileDownloaderGenerator(String prefix) {
		pid = Configuration.getPid(prefix + "." + PAR_PROT);
		fid = Configuration.getPid(prefix + "." + PAR_FILEPROT);
		bandwidth = Configuration.getInt(prefix + "." + PAR_BAND);
		
		Node n = Network.get(0);
		FileTransferProtocol fp = (FileTransferProtocol) n.getProtocol(fid);		
		
		downloader = new FileDownloader(fp.fileSize, bandwidth);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see peersim.core.Control#execute()
	 */
	@Override
	public boolean execute() {

		Node n = Network.get(0);
		FileTransferProtocol fp = (FileTransferProtocol) n.getProtocol(fid);
		int size = Network.size();
		Node sender, target;
		int i = 0;
	
		
		for (i=0; i < fp.availableSource; i++){
			sender = Network.get(CommonState.r.nextInt(size));	
			target = FileObject.addresses[i];
			if (sender == null || sender.isUp() == false || target == null
				|| target.isUp() == false){
				
			} else {
				FileMessage message = new FileMessage(sender,
						((SimpleChordProtocol) target.getProtocol(pid)).chordId, downloader);
				if(count<fp.availableSource)
				EDSimulator.add(10, message, sender, fid);
				count++;
			
			}
		}

		
		/*
		do {

			sender = Network.get(CommonState.r.nextInt(size));
			target = FileObject.addresses[i];
			i++;
			
			if (i > fp.availableSource -1) {
				sender = null;
				target = null;
				break;
			}
		} while (sender == null || sender.isUp() == false || target == null
				|| target.isUp() == false);
		if (sender == null && target == null) {

		} else {

		}
		*/
		return false;
	}

}
