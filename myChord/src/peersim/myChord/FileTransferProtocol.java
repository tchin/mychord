package peersim.myChord;

import java.math.BigInteger;
import java.util.HashMap;

import peersim.config.Configuration;
import peersim.core.Node;
import peersim.edsim.EDProtocol;
import peersim.transport.Transport;

public class FileTransferProtocol  implements EDProtocol{
	public String prefix;	
	public Parameters p;
	public FileObject obj;
	
	public int availableSource = 0;	
	public long fileSize = 0;	
//	public int numConnection = 0;
	public boolean step = false;
	// Configuration parameter
	private static final String PAR_TRANSPORT = "transport";	
	private static final String PAR_CHORD = "protocol";
	private static final String PAR_ASOURCE = "availableSource";
	private static final String PAR_IDLENGTH = "fileSize";
//	private static final String PAR_CONNECTION = "connection";
	
	
	//
	int fails=0;
	
	int cid =0;
	public FileTransferProtocol(String prefix) {
		this.prefix = prefix;
		p = new Parameters();
		p.tid = Configuration.getPid(prefix + "." + PAR_TRANSPORT);
		p.pid =Configuration.getPid(prefix + "." + PAR_CHORD);
		availableSource = Configuration.getInt(prefix + "." + PAR_ASOURCE);		
		fileSize = Configuration.getLong(prefix + "." + PAR_IDLENGTH);		
//		numConnection = Configuration.getInt(prefix + "." + PAR_CONNECTION);		
	}
	
	
	public Object clone() {
		FileTransferProtocol fp = new FileTransferProtocol(prefix);
		String val = BigInteger.ZERO.toString();
		if (fp.obj != null)
		fp.obj = new FileObject(obj.hashNumber,obj.size,obj.availableSource);
		return fp;
	}
	
	
	
	@Override
	public void processEvent(Node node, int pid, Object event) {
		// TODO Auto-generated method stub

	
		if (event.getClass() == FileMessage.class) {
			SimpleChordProtocol cp = ((SimpleChordProtocol)node.getProtocol(cid));
			FileMessage message = (FileMessage) event;			
			message.increaseHopCounter();
			BigInteger target = message.getTarget();
			Transport t = (Transport) node.getProtocol(p.tid);
			Node n = message.getSender();
			if (target == ((SimpleChordProtocol) node.getProtocol(cid)).chordId) {
				t.send(node, n, new FileFinalMessage(message.getHopCounter(),message.getDownloader()), p.pid);
			}
			if (target != ((SimpleChordProtocol) node.getProtocol(cid)).chordId) {
				// routing
				
				Node dest = cp.find_successor(target);
				if (dest == null ){
					fails++;
				} else {
					dest = ((SimpleChordProtocol)dest.getProtocol(cid)).predecessor;
					if (dest.isUp() && ((SimpleChordProtocol)dest.getProtocol(cid)).chordId.compareTo(target)==0){
						t.send(message.getSender(), dest, message, p.pid);
//					System.out.println("sending from" + chordId + "to "+ target +" through "
//							+ ((SimpleChordProtocol)dest.getProtocol(pid)).chordId);
					} else {
						fails++;
					}
				}
	

			}			
		
			

		} else if (event.getClass() == FileFinalMessage.class){
	
			FileFinalMessage message = (FileFinalMessage) event;
			if (step ==false){
				message.getDownloader().increaseStep();
				step = true;
			}
			
			
		}
	}

}
