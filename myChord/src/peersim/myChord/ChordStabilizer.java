package peersim.myChord;

import java.math.BigInteger;

import peersim.config.Configuration;
import peersim.core.CommonState;
import peersim.core.Control;
import peersim.core.Network;
import peersim.core.Node;

public class ChordStabilizer  implements Control{
	private final String prefix;
	private static final String PAR_PROT = "protocol";
	private final int pid;		
	public ChordStabilizer(String prefix) {
		super();
		this.prefix = prefix;
		this.pid = Configuration.getPid(prefix + "." + PAR_PROT);
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean execute() {
		// TODO Auto-generated method stub
		for (int i = 0; i < Network.size(); i++) {
			Node node = (Node) Network.get(i);
			if (node.isUp()){
				SimpleChordProtocol cp = (SimpleChordProtocol) node.getProtocol(pid);
				cp.stabilize(node);
				cp.stabilizations++;
				cp.fixFingers();
				
			}
			//cp.printFingers();
		}
		return false;
	}

}
