package peersim.myChord;

import java.math.BigInteger;

import peersim.core.Node;

public class FileObject {
	public long size;
	
	public BigInteger hashNumber;
	
	public int availableSource;
	
	static public Node addresses[];
	
	static public int varAddress = 0;
	
	public FileObject(BigInteger number, long size, int availableSource){
		this.hashNumber = number;
		this.size = size;
		this.availableSource = availableSource;
		this.addresses = new Node[availableSource];
	}
	
	public Object clone() {
		FileObject fp = new FileObject(hashNumber, size, availableSource);
		return fp;
	}
	static public void reset(){
		varAddress = 0;
	}
	static public void addLocation(Node i){
		addresses[varAddress] = i;
		varAddress++;
	}
	
}
