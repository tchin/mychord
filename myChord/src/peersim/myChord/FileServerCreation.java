package peersim.myChord;

import java.math.BigInteger;
import java.util.Random;

import peersim.config.Configuration;
import peersim.core.CommonState;
import peersim.core.Control;
import peersim.core.Network;
import peersim.core.Node;

public class FileServerCreation implements Control {
	private int pid = 0;
	private int fid = 0;

	// Configuration parameter
	private static final String PAR_PROT = "protocol";
	private static final String PAR_FILEPROT = "fileProtocol";

	public FileServerCreation(String prefix) {
		pid = Configuration.getPid(prefix + "." + PAR_PROT);
		fid = Configuration.getPid(prefix + "." + PAR_FILEPROT);

	}

	@Override
	public boolean execute() {
		FileObject.reset();
		for (int i = 0; i < Network.size(); i++) {
			Node node = (Node) Network.get(i);
			FileTransferProtocol cp = (FileTransferProtocol) node
					.getProtocol(fid);
			cp.p.pid = fid;
			cp.cid = pid;
		}
		Random generator = new Random(CommonState.r.nextLong());
		Node n = Network.get(0);
		FileTransferProtocol fp = (FileTransferProtocol) n.getProtocol(fid);
		SimpleChordProtocol cp = (SimpleChordProtocol) n.getProtocol(pid);
		FileObject obj = new FileObject(new BigInteger(cp.m, CommonState.r),
				fp.fileSize, fp.availableSource);
		for (int i = 0; i < fp.availableSource; i++) {
			// choise a node from network which is up with no obj is set

			int numberOfTry = 0;
			do {
				n = Network.get(generator.nextInt(Network.size()));
				numberOfTry++;
				if (numberOfTry > 10) {
					break;
				}
			} while (n == null || n.isUp() == false
					|| ((FileTransferProtocol) n.getProtocol(fid)).obj != null);
			if (n == null) {
				System.out.println("No node can save the file");
			} else {
				fp = (FileTransferProtocol) n.getProtocol(fid);
				fp.obj = (FileObject) obj.clone();
				FileObject.addLocation(n);
			}

			// TODO Auto-generated method stub

		}
		return false;
	}
}
