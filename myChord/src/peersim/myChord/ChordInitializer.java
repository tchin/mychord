package peersim.myChord;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigInteger;
import java.util.Random;
import peersim.config.Configuration;
import peersim.core.CommonState;
import peersim.core.Network;
import peersim.core.Node;
import peersim.dynamics.NodeInitializer;

//class that is used to initialized node to join the network
public class ChordInitializer implements NodeInitializer {
	  FileWriter fstream;
	  BufferedWriter out;
	private static final String PAR_PROT = "protocol";
	private static final String PAR_FILEPROT = "fileProtocol";
	private int pid = 0;
	private int fid = 0;
	private SimpleChordProtocol cp;
	private FileTransferProtocol fp;

	public ChordInitializer(String prefix) {
		pid = Configuration.getPid(prefix + "." + PAR_PROT);
		fid = Configuration.getPid(prefix + "." + PAR_FILEPROT);		
		
		try {
			fstream = new FileWriter("out.txt");
			out = new BufferedWriter(fstream);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	//when initialize is call , the node n is already
	//existed 
	public void initialize(Node n) {
		cp = (SimpleChordProtocol) n.getProtocol(pid);
		fp = (FileTransferProtocol) n.getProtocol(fid);
		join(n);
	}

	 public void join(Node myNode) {
		Random generator = new Random(CommonState.r.nextLong());
		cp.predecessor = null;
		cp.m = ChordCreation.idLength;
		cp.p.pid = pid;
		fp.p.pid = fid;
		fp.cid = pid;
		// search a node to join randomly from the network
		//this action would be risky due to n which is not 
		//initialize is choosen to join network
		//this would be not recommended if network size is small
	
		Node n;
		int numberOfTry = 0;
		do {
			n = Network.get(generator.nextInt(Network.size()));
			numberOfTry++;
			if (numberOfTry > 10){
				break;
			}
		} while (n == null || n.isUp() == false || 
				((SimpleChordProtocol)n.getProtocol(pid)).joinState == false );
		if (n == null) {
			n = Network.get(0);
		}
	
		Node successor ;

		SimpleChordProtocol cpRemote = (SimpleChordProtocol) n.getProtocol(pid);		
		cp.chordId = new BigInteger(cp.m, CommonState.r);		
		do {
			boolean detect = false;
			do {
				for (int i = 0; i < Network.size(); i++) {
					Node node = (Node) Network.get(i);
					SimpleChordProtocol cp2 = (SimpleChordProtocol) node
							.getProtocol(pid);				
					detect = false;
					if (node.isUp() && cp2.chordId.compareTo(cp.chordId) == 0){
						detect = true;
						cp.chordId = new BigInteger(cp.m, CommonState.r);
						break;
					}
				}
			} while (detect == true);

		
		
	//		System.out.println("Calling " + cpRemote.chordId);
		successor = cpRemote.find_successor(cp.chordId);
		//if successor have same node id with the node in the network
		//regenerate the chord id	
		} while (((SimpleChordProtocol) successor.getProtocol(pid)).chordId.compareTo(cp.chordId)==0 && successor.isUp()==true);
		
		try {
			out.write("Add chord id" + cp.chordId + " to successor " + ((SimpleChordProtocol) successor.getProtocol(pid)).chordId +"\n");
			out.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		cp.setNode(myNode);
		cp.fails = 0;
		cp.stabilizations = 0;
		cp.varSuccList = cpRemote.varSuccList;
		cp.succLSize = cpRemote.succLSize;
		cp.successorList = new Node[cp.succLSize];
		cp.successorList[0] = successor;
		cp.fingerTable = new Node[cp.m];
		try {
			cp.updateSuccessorList();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		cp.stabilizations++;
		cp.stabilize(myNode);
		cp.fixFingers();
		cp.joinState = true;
	}
}
