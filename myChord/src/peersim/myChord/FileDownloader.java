package peersim.myChord;

public class FileDownloader {
	public double size;
	long start;
	long end = System.currentTimeMillis();
	public double step = 0; // represent how fast the download speed
	static public MyWriter w;
	static long number=0;
	static int z = 0;
	public int bandwidth = 0;
	{
		if (w == null)
		w = new MyWriter("out5.txt");		
	}
	public FileDownloader(long size, int bandwidth) {
		this.size = size;
		Thread a = new Thread(new Reduce(this, 5));
		this.bandwidth = bandwidth;	
		a.start();
		start = System.currentTimeMillis();

	}

	synchronized public void increaseStep() {
//		w.write("increase");
		bandwidth --;
		if (bandwidth > 0)
			step=step + 1;
	}
	synchronized public void increaseStep(double num) {
//		w.write("increase");
		bandwidth --;
		if (bandwidth > 0)
			step=step + num;
	}	

}

class Reduce implements Runnable {
	FileDownloader d;
	int sleepTime;

	public Reduce(FileDownloader a, int sleepTime) {
		super();
		// TODO Auto-generated constructor stub
		d = a;
		this.sleepTime = sleepTime;

	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		while (d.size > 0) {
			d.size = d.size - d.step;
			if (d.size < 1) {
				d.end = System.currentTimeMillis();
				long num = d.end - d.start;
				FileDownloader.number += (num);
				d.w.write("duration " +(num) + "mm" + "\n");
				d.w.write("sum " +(FileDownloader.number / 5) + "mm" + "\n");
			}
			try {
				Thread.sleep(sleepTime);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

}